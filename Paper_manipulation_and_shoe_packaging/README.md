# Robotic Paper Manipulation and Shoe Packaging Method

## Introduction

In this project, we propose a solution for a shoe packaging task, involving objects of different dimensions and flexibilities, such as paper, shoes, and boxes. 
The flexibility of paper complicates paper manipulation tasks, such as grasping, placing, and folding. 
Furthermore, the shoes must be placed in a limited space in a box, in a specific arrangement. 
Fig.1 shows a scene wherein shoes are being packaged.

<div align="center">
<!-- <img src="https://gitlab.com/neutron-nuaa/robot-arm/-/blob/main/Grasp_system_based_on_vision/Grasp_system.png" width="200"> -->
<img src="https://gitlab.com/neutron-nuaa/robot-arm/-/raw/main/Paper_manipulation_and_shoe_packaging/1.jpg" width="400">

**Fig.1:** Shoe packaging scene. The manipulated objects include paper, shoes, and boxes.
</div>

The objects manipulated in this project were a piece of paper of size 77cm * 32cm, a pair of shoes of size 40 (EUR), and a box of size 30cm * 22cm * 11cm. 
A fourfinger soft gripper was used in this study. 
The robot arm was UR5e and the camera was Intel RealSense D415.

## Shoe packaging method

The shoe packaging method consists of perception, manipulation, and task planning. 
Perception deals with estimating the locations and poses of objects. 
In the manipulation step, an appropriate paper grasping strategy is chosen, and the shoe manipulation process is studied, considering the grasping and placement. 
Finally, the entire task was completed in five steps: paper grasping and placement, first shoe grasping and placement, paper folding, second shoe grasping and placement, and box closing.

### Perception

In the first step, YOLO is used to obtain the object categories and regions, which form the basis of the subsequent object pose estimation.

<div align="center">
<img src="https://gitlab.com/neutron-nuaa/robot-arm/-/raw/main/Paper_manipulation_and_shoe_packaging/2.jpg" width="800">

**Fig.2:** The perception part of the shoe packaging method. 
YOLO is used to obtain the different object areas, and the object poses are calculated according to their features. 
As in the case of paper, we first obtain the segmentation areas and contours. Four right angle corners are obtained for the minimum external rectangle. The grasping position can be calculated from these corners, and the orientation of the object can be calculated from the slope of the minimum external rectangle. 
The shoe contours can be obtained from the background color and can provide the shoe’s minimum external rectangle. The position of the shoe can be obtained from the minimum external rectangle. The shoe orientation can be calculated from the depth data of the shoe and its center. 
Similar to the paper pose estimation, the box contours can be obtained. However, the box has more than four corners, from different perspectives, owing to its special structure. Both its minimum external rectangle and convex hull are used to obtain its four right angle corners. Then, the poses of the box’s three main points are obtained from these corners.
</div>

#### Paper pose estimation

Grasping the paper at the midpoint of the short edge is beneficial for placing and flattening. 
As shown in Fig.2, based on the paper bounding box obtained during object detection, the paper segmentation area and contours can be obtained. 
The corners of the paper are the four corners of the minimum external rectangle of the paper’s contour. 

Next, the paper pose is estimated from these four corners. 
First, we find the paper corner $`(x_1, y_1)`$ that is the closest to the center of the camera view and a second corner $`(x_2, y_2)`$, which is the closest to the first corner. 
The grasp point $`(x, y, z)`$ of the paper is the midpoint of these two corners. 
The paper orientation can be calculated from the slope of the line joining the two corners. 
Because the gripper is perpendicular to the table, the orientation can be expressed as RPY $`(0, 0, Yaw)`$. The yaw angle can be obtained using equation (1).

```math
\begin{align}
    Yaw = arctan\frac{y_2 - y_1}{x_2 - x_1}
\end{align}
```

#### Shoe pose estimation

The poses of the shoes are used not only for grasping, but also for placing them within the box, in opposite directions.
The position and orientation of the shoe is sufficient for this task.
Similar to the tasks for known objects, it is more convenient to obtain the object's pose from its features. 
The shoe's pose is estimated from its geometric features.

Based on the bounding box of the shoe, the shoe contour is obtained according to the background color, and the minimum external rectangle of the shoe is determined. 
The shoes' grasp position $`(x, y, z)`$ is the center of this rectangle. 
Considering the shoes' opposite-direction placement within the box, the shoe's slope alone will not be sufficient.
The slope $`k`$ of the short edge of the minimum external rectangle and the vector from the highest point $`(x_2, y_2)`$ to the center $`(x_1, y_1)`$ determine the orientation of the shoe. 
Similar to the paper’s grasp pose, the orientation of the shoe’s pose is RPY $`(0, 0, Yaw)`$. The yaw range is represented by equation (2).

```math
\begin{align}
Yaw =
    \begin{cases}
    (-90^{\circ}, 0], & k > 0, y_2 - y_1 > 0 \\
    (-180^{\circ}, -90^{\circ}], & k \leq 0, y_2 - y_1 > 0 \\
    (-270^{\circ}, -180^{\circ}], & k > 0, y_2 - y_1 < 0 \\
    (-360^{\circ}, -270^{\circ}], & k \leq 0, y_2 - y_1 < 0 
    \end{cases}
\end{align}
```
#### Box pose estimation

The structure of a box is symmetric, and at least three key point poses are required for shoe packaging. 
As shown in Fig.2, a larger number of steps is required to obtain the poses of the three expected points (point1, point2, point3) because of the complex structure of the box with a lid.

Similar to the paper pose estimation, the box contours are obtained. 
The difference is that the four right angle corners cannot be obtained from the external rectangle of the box contour, because a box with a lid has more than four corners from different perspectives (Fig.2). 
However, among all corners, the four right angle corners are the closest to the four corners of the box's external rectangle. 
Therefore, the box's minimum external rectangle and convex hull, which includes all the box corners, are used to obtain the four right angle corners accurately. 
Point1 and point2 can be obtained based on the four corners. The depth data of a trisection point on the line joining point1 and point2 can determine which point is located on the lid of the box. 
Point3 is the highest point on the line between point1 and point2. 

### Manipulation

#### Paper grasping strategies

Strategy 1 (Fig.3 (a) and (e)) is based on the pinch skill and is called grasping from the top of the paper. 
When the gripper moves up, the fingers will make a wrinkle, the length of which is the grasp area, G (Fig.3 (a)). 
The paper is grasped when the gripper is completely closed (Fig.3 (e)).

Fig.3 (b) and (f) show Strategy 2, also based on the pinch skill; however, the paper is grasped from the edge. 
This strategy has a slide area S.
The length of the grasp area G remain constant. The grasp result is shown in Fig.3 (f).

Strategy 3, which involves grasping with the help of the environment, is shown in Fig.3 (c) and (g). This strategy uses other objects in the environment. 
This strategy has two important parameters, the wrap area W and angle of the gripper $`\theta`$ (Fig.3 (c)). 

Fig.3 (d) and (h) show Strategy 4: grasping by exploiting the table edge. The slide skill is used to push the paper over the edge of the table and the pinch skill is used to grasp it. 
This strategy ensures high surface quality, as shown in Fig.3 (h). 

<div align="center">
<!-- <img src="https://gitlab.com/neutron-nuaa/robot-arm/-/blob/main/Grasp_system_based_on_vision/Grasp_system.png" width="200"> -->
<img src="https://gitlab.com/neutron-nuaa/robot-arm/-/raw/main/Paper_manipulation_and_shoe_packaging/3.jpg" width="600">

**Fig.3:** Paper grasp strategies. (a) Grasping from the top of the paper. (b) Grasping from the paper edge. (c) Grasping with the help of the environment. (d) Grasping by exploiting the table edge. (e)–(h) Grasping with the four grasping strategies and the paper's state in each case.
</div>

#### Shoe manipulation with gravity

As shown in Fig.4 (a), there are two poses for grasping the shoe. 
Grasp pose (1) grasps the shoe at the center. The soft gripper can adapt to the shoe's irregular shape.
Grasp pose (2) requires a greater clamping force than grasp pose (1). If the soft gripper uses pose (2), the shoe's pose will change owing to the torque caused by gravity because of the small clamping force. 
Therefore, grasp pose (1) is more appropriate for soft grippers, and grasp pose (2) is more appropriate for rigid grippers.

Because of the limited space within the box, placing the shoes while avoiding collisions between the gripper and the box is a challenge. In this study, gravity and the contacts between objects were exploited to place the shoes. 
As shown in Fig.4 (b), the shoe is placed on the box edge at an eccentric distance from the shoe center.
When the gripper is open, the contacts and gravity create a moment that helps the shoe fall into the box in an expected pose. 
The success rate was tested ten times at eccentricities of 5, 10, 15, 20, and 25 mm. 
Fig.4 (c) shows the success rates under different eccentricities. When the eccentricity is less than 20 mm, the success rate is $`100\%`$.

<div align="center">
<!-- <img src="https://gitlab.com/neutron-nuaa/robot-arm/-/blob/main/Grasp_system_based_on_vision/Grasp_system.png" width="200"> -->
<img src="https://gitlab.com/neutron-nuaa/robot-arm/-/raw/main/Paper_manipulation_and_shoe_packaging/4.jpg" width="600">

**Fig.4:** Shoe grasping and placement. (a) Two grasp poses for a shoe ((1), (2)). A is the shoe's center line. (b) Placement of the shoe on the box edge at eccentric distance, which is the distance between the shoe's center line A and the box edge B. (c) Success rates for different eccentric distances.
</div>

### Task planning

Based on the human packaging process, the shoe packaging task was divided into five steps: paper grasping and placement, first shoe grasping and placement, paper folding, the other shoe grasping and placement, and box closing.

**Paper grasping and placement.** In shoe packaging, there are no strict requirements for the paper's surface quality. 
Grasping from the paper edge (Fig.3 (b)) is the best grasping strategy to grasp and flatten the paper using gravity. 
Because paper is light and hysteretic in movement, the robot must stop at a waypoint, when placing the paper, to wait for the paper to reach. 
In addition, the paper in the box must be pressed to facilitate the placement of the shoes.

**Shoe grasping and placement.** The robot uses grasp pose (2) (Fig.4 (a)) to grasp the first detected shoe, and then places it eccentrically on the box edge. 
The orientation of the shoe is aligned with point2 (Fig.2) on the box. 
The eccentricity is set to 10 mm. When opening the gripper, the shoe will drop into the box in the expected pose.

**Paper folding.** The grasping strategy is grasping from the paper edge. 
The trajectory for folding the paper is an arc trajectory with point2 (Fig.2) as the center.

**The other shoe manipulation.** The robot grasps the second shoe and places it eccentrically at point3 (Fig.2) in the box. 
Regardless of which shoe was placed first, make sure that both the shoes face opposite directions.

**Box closing:** When closing the box, point1 on the box is the grasping point. 
As shown in Fig.2, the trajectory of closing the box is also an arc trajectory with point3 as the center and R, which is the distance between point1 and point3, as the radius. 
Simultaneously, the orientation of the gripper is changed. 
Both the grasp position and grasp orientation are functions of the rotation angle of the box cover.

## Contents

The project is organized by these folders, which are all ROS packages, including packages and meta-pakages. 

**UR robot (UR5e)**
- **Universal_Robots_ROS_Driver:** UR robot driver meta-package.
- **fmauch_universal_robot:** UR robot description meta-package.

Tutorial: https://github.com/UniversalRobots/Universal_Robots_ROS_Driver

**Camera (Realsense D415)**
- **realsense-ros:** a package for using Intel RealSense cameras with ROS.
- **ddynamic_reconfigure:** a package that allows modifying parameters of a ROS node of the camera.

Tutorial: http://neutron.manoonpong.com/perception-vision-realsense-set-up-tutorial/

**Eye to hand calibration**
- **easy_handeye:** an automated, hardware-independent Hand-Eye Calibration package.
- **aruco_ros:** a software package and ROS wrappers of the Aruco Augmented Reality marker detector library.
- **vision_visp:** a package which provides visual servoing platform algorithms as ROS components. 

**Perception**
- **darknet_ros:** a ROS package developed for object detection in camera images.
- **ur_vision:** the perception algorithm for this shoe packaging task.

**Task plannning**
- **ur_demo:** demos about path planning using MoveIt.
- **ur_grasping:** the task panning algorithm for this shoe packaging task.

## The implementation of the project

First create a new ROS workspace for this project. And then clone all the packages into the ROS workspce. 
Of course, you can also install the packages step by step fllowing tutorial links above (recommend) or the tutorial https://gitlab.com/neutron-nuaa/robot-arm/-/tree/main/Grasp_system_based_on_vision. 
Make sure that all packages are compiled successfully. 

### Eye to hand calibration

```
# run the calibration software
$ roslaunch easy_handeye eye_to_hand_calibration.launch

# Check calibration results
$ roslaunch easy_handeye publish.launch
$ rosrun tf tf_echo /base /camera_color_frame

```

### Steps to run the project

```
# run the perception algorithm
$ roslaunch ur_vision ur_vision_demo.launch

# publish the result of calibration
$ roslaunch easy_handeye publish.launch

# start the robot arm
$ roslaunch ur_robot_driver ur5e_work_all.launch

# task execution
$ rosrun ur_grasping graspingDemo

```

## Reference

Video: www.manoonpong.com/Shoepackaging/video.mp4.

Paper: Robotic Shoe Packaging Strategies Based on a Single Soft-Gripper System and Extrinsic Resources. (under review)

If you have any questions/doubts about how to implement this project on your computer, you are welcome to raise issues and email to me. My email address is dongyi@nuaa.edu.cn.
