/***********************************************************************
Author: Dong Yi
Contact: dongyi@nuaa.edu.cn
Update: 22/02/2022
***********************************************************************/

#ifndef ur_VISION_MANAGER
#define ur_VISION_MANAGER

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseArray.h>
#include "tf/transform_datatypes.h" //RPY transformation
#include <math.h>

#include <darknet_ros_msgs/BoundingBox.h>
#include <darknet_ros_msgs/BoundingBoxes.h>
#include <darknet_ros_msgs/ObjectCount.h>

class VisionManager
{
  public:
	
	//VisionManager Constructor Function
	VisionManager(ros::NodeHandle n_);

	/*
	 * @brief      Gets the 2d location of object in camera frame
	 *
	 * @param[in]  img   The image
	 * @param      x     x postion of the object
	 * @param      y     y position of the object
	 */
	void get2DLocation(const sensor_msgs::ImageConstPtr &msg, float &x, float &y);

	/**
	 * @brief      imageCb is called when a new image is received from the camera
	 *
	 * @param[in]  msg   Image received as a message
	 */
	void imageCb(const sensor_msgs::ImageConstPtr &msg);

  private:
	
	//number of detected objects
	void objectNumber(const darknet_ros_msgs::ObjectCount::ConstPtr &t);

	//get boundingbox of detected objects
	void objectBox(const darknet_ros_msgs::BoundingBoxes::ConstPtr &msg);
	
	/**
 	 * @brief      paper grasp pose estimation
 	 *
 	 * @param      pixel_x  postion of the object in x-pixels
 	 * @param      pixel_y  positino of the object in y-pixels
 	 */
	void detectPaper(const sensor_msgs::ImageConstPtr &msg, float &pixel_x, float &pixel_y, cv::Rect &paperBox);

	void detectShoe(const sensor_msgs::ImageConstPtr &msg, float &pixel_x, float &pixel_y, cv::Rect &shoeBox);

	void detectBox(const sensor_msgs::ImageConstPtr &msg, float &pixel_x, float &pixel_y, cv::Rect &boxBox);

	//get the distance between two points
	float getDistance (cv::Point2f pointA, cv::Point2f pointB );

	//get the closest point between the pointC and corners (points)
	cv::Point2f getClosestPoint (cv::Point2f pointC, std::vector < cv::Point2f > corners );

	void cameraInfoCb( const sensor_msgs::CameraInfo &msg );
	void imageDepthCb( const sensor_msgs::ImageConstPtr &msg );

	//pixel coordinate to camera coordinate
	geometry_msgs::PointStamped coordinateMap(cv::Point2f graspPoint);

	//find the sharp corners of the contour
	std::vector <cv::Point2f>  contourCorner(std::vector<std::vector<cv::Point>> &contours);

	//void getRotatedRect(cv::Mat image, cv::RotatedRect rotatedRect);

    //Image transfer pointer
	cv_bridge::CvImagePtr cv_ptr_;

  	//Handle to the image
	image_transport::ImageTransport it_;

	//Image publisher and subscriber
	image_transport::Subscriber image_sub_;
	image_transport::Subscriber	image_sub_depth;
	
	image_transport::Publisher image1_pub_;
	image_transport::Publisher image2_pub_;
	
	//Topic publisher and subscriber
	ros::Subscriber objectNumber_sub1;
	ros::Subscriber objectBox_sub2;
	ros::Subscriber camera_info_sub_;

	ros::Publisher paper_graspPose_pub_;
	ros::Publisher shoe_graspPose_pub_;
	ros::Publisher box_graspPose_pub_;

	cv::Mat	colorImage;
	cv::Mat	depthImage = cv::Mat::zeros( 1280, 720, CV_16UC1 );//change this according to the depth image size you receive
	sensor_msgs::CameraInfo camera_info;

	// Locale Variables
	int i = 0; //i is the number of objects
	int k = 0; //k is the number of corners
	bool writeImage = false; //write image bool value

	//detected objects' boundingbox
	cv::Rect paperBox, shoeBox, boxBox;

	//output poses
	geometry_msgs::PoseStamped	paper_graspPose, shoe_graspPose;
	geometry_msgs::PoseArray box_graspPose;
	geometry_msgs::Pose	box_graspPose1, box_graspPose2, box_graspPose3;

};

#endif
