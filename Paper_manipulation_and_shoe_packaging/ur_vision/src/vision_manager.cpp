/***********************************************************************
Author: Dong Yi
Contact: dongyi@nuaa.edu.cn
Update: 22/02/2022
***********************************************************************/

#include "ur_vision/vision_manager.h"

//////////////////////////////////////////////////////////////////////////////////////
//***************************Constructor Function***********************************//
//////////////////////////////////////////////////////////////////////////////////////

VisionManager::VisionManager(ros::NodeHandle n_) : it_(n_)
{
    //get objects' area
    objectNumber_sub1 = n_.subscribe("/darknet_ros/found_object", 1, &VisionManager::objectNumber, this);
    objectBox_sub2 = n_.subscribe("/darknet_ros/bounding_boxes", 1, &VisionManager::objectBox, this);
    
    //aligned depth to color and get the depth data
    image_sub_depth = it_.subscribe( "/camera/aligned_depth_to_color/image_raw", 1, &VisionManager::imageDepthCb, this );
    camera_info_sub_ = n_.subscribe( "/camera/aligned_depth_to_color/camera_info", 1, &VisionManager::cameraInfoCb, this );
    
    //subscribe to input image
    image_sub_  = it_.subscribe("/camera/color/image_raw", 1, &VisionManager::imageCb, this);

    //publish object pose
    paper_graspPose_pub_ = n_.advertise<geometry_msgs::PoseStamped>("/paper_graspPose", 1);
    shoe_graspPose_pub_ = n_.advertise<geometry_msgs::PoseStamped>("/shoe_graspPose", 1);
    box_graspPose_pub_ = n_.advertise<geometry_msgs::PoseArray>("/box_graspPose", 1);

}

//////////////////////////////////////////////////////////////////////////////////
//***************************obtain boundingbox*********************************//
//////////////////////////////////////////////////////////////////////////////////

//number of detected objects
void VisionManager::objectNumber(const darknet_ros_msgs::ObjectCount::ConstPtr &t)
{
    i = t->count;
    std::cout<<"ObjectNumber:" << i <<std::endl;
}

//obtain paper/shoe/box area
void VisionManager::objectBox(const darknet_ros_msgs::BoundingBoxes::ConstPtr &msg)
{
    //the area can only be obtained after the object is detected
    for(int j = 0; j < i; j++)
    {        
        //cv::Rect paperBox0, shoeBox0, boxBox0;
        if(msg->bounding_boxes[j].Class == "paper")
        {
            paperBox.x = msg->bounding_boxes[j].xmin;
            paperBox.y = msg->bounding_boxes[j].ymin;
            paperBox.width = msg->bounding_boxes[j].xmax - msg->bounding_boxes[j].xmin;
            paperBox.height = msg->bounding_boxes[j].ymax - msg->bounding_boxes[j].ymin;
	        //paperBox = paperBox0 + cv::Size(100, 100); //larger area
            std::cout<< paperBox <<std::endl;
        }
        else if(msg->bounding_boxes[j].Class == "shoe")
        {
            shoeBox.x = msg->bounding_boxes[j].xmin;
            shoeBox.y = msg->bounding_boxes[j].ymin;
            shoeBox.width = msg->bounding_boxes[j].xmax - msg->bounding_boxes[j].xmin;
            shoeBox.height = msg->bounding_boxes[j].ymax - msg->bounding_boxes[j].ymin;
            //shoeBox = shoeBox0 + cv::Size(100, 100);
            //cout<< shoeBox <<endl;
        }
        else
        {
            boxBox.x = msg->bounding_boxes[j].xmin;
            boxBox.y = msg->bounding_boxes[j].ymin;
            boxBox.width = msg->bounding_boxes[j].xmax - msg->bounding_boxes[j].xmin;
            boxBox.height = msg->bounding_boxes[j].ymax - msg->bounding_boxes[j].ymin;
            //boxBox = boxBox0 + cv::Size(100, 100);
            //cout<< paperBox <<endl;
        }
        std::cout<< "There is detected object"<<std::endl;
    }

    //output the area of the detected objects
    /*
    cout<<"Bouding Boxes (header):" << msg->header <<endl;
    cout<<"Bouding Boxes (image_header):" << msg->image_header <<endl;
    cout<<"Bouding Boxes (Class):" << msg->bounding_boxes[0].Class <<endl;
    cout<<"Bouding Boxes (xmin):" << msg->bounding_boxes[0].xmin <<endl;
    cout<<"Bouding Boxes (xmax):" << msg->bounding_boxes[0].xmax <<endl;
    cout<<"Bouding Boxes (ymin):" << msg->bounding_boxes[0].ymin <<endl;
    cout<<"Bouding Boxes (ymax):" << msg->bounding_boxes[0].ymax <<endl;
    */
}

/////////////////////////////////////////////////////////////////////////////////////
//********************************grasp pose***************************************//
/////////////////////////////////////////////////////////////////////////////////////

//get colorImage
void VisionManager::imageCb(const sensor_msgs::ImageConstPtr &msg)
{
    ROS_INFO_STREAM("Processing the Image to locate the Object...");
    
    cv_bridge::CvImagePtr cv_ptr;
    try 
    {
        cv_ptr		= cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
        colorImage	= cv_ptr->image;
    } 
    catch ( cv_bridge::Exception &e ) 
    {
        ROS_ERROR( "cv_bridge exception: %s", e.what() );
        return;
    }

    //ROS_INFO("Image Message Received");
    float obj_x, obj_y;
    get2DLocation(msg, obj_x, obj_y);

    //Temporary Debug
    //std::cout<< " X-Co-ordinate in Camera Frame :" << obj_x << std::endl;
    //std::cout<< " Y-Co-ordinate in Camera Frame :" << obj_y << std::endl;
}

//go to object pose estimation function
void VisionManager::get2DLocation(const sensor_msgs::ImageConstPtr &msg, float &x, float &y)
{
    if (paperBox.width > 0 && paperBox.height >0)
    {
        ROS_INFO_STREAM("Go to detectPaper function...");
        detectPaper(msg, x, y, paperBox);
    }

    if (shoeBox.width > 0 && shoeBox.height >0)
    {
        ROS_INFO_STREAM("Go to detectShoe function...");
        detectShoe(msg, x, y, shoeBox);
    }

    if (boxBox.width > 0 && boxBox.height > 0)
    {
        ROS_INFO_STREAM("Go to detectBox function...");
        detectBox(msg, x, y, boxBox);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//paper grasp pose estimation
//Method1 corner detection (grayImage)
//Method2 threshold boundingRect (binaryImage)
void VisionManager::detectPaper(const sensor_msgs::ImageConstPtr &msg, float &pixel_x, float &pixel_y, cv::Rect &paperBox)
{
    ROS_INFO_STREAM("Detecting paper now...");
    cv::Mat denoiseImage1, denoiseImage2, paperHsv, mask;
    std::vector < cv::Point2f > cornersA(4), cornersB(4);
    cv::Point2f rectVertex[4], graspPoint, paper_rectCenter;
    cv::Rect paperRect; 
    cv::RotatedRect paper_rotatedRect; 
    std::vector< std::vector< cv::Point > > contours;

    float centerx, centery;
    double r, p, y;
    double maxarea = 0;  
    int maxAreaIdx = 0;  

    //************************************************************************************

    //get the center of paper area
    paper_rectCenter.x = paperBox.x + paperBox.width/2;
    paper_rectCenter.y = paperBox.y + paperBox.height/2;
    
    cv::medianBlur(colorImage, denoiseImage1, 3);
    denoiseImage2 = denoiseImage1;
    
    //paper segmentation
    cv::floodFill
    (
        denoiseImage1, 
        paper_rectCenter, 
        cv::Scalar(0, 0, 255), 
        &paperRect, 
        cv::Scalar(5, 5, 5),
        cv::Scalar(5, 5, 5)
    );
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/paperSegmentation.jpg", denoiseImage1);
        cv::waitKey(500);
    }

    //************************************************************************************
    
    //get paper area
    cv::cvtColor( denoiseImage2, paperHsv, cv::COLOR_BGR2HSV);
    cv::Scalar scalarL = cv::Scalar(0, 43, 46); //red color
    cv::Scalar scalarH = cv::Scalar(1, 255, 255);
    
    cv::inRange(paperHsv, scalarL, scalarH, mask); //mask is a binary image
    dilate(mask, mask, cv::Mat());

    //find the paper contour
    cv::findContours
    (
        mask,
        contours,
        cv::noArray(),
        cv::RETR_EXTERNAL,
        cv::CHAIN_APPROX_SIMPLE
    );
    
    mask = cv::Scalar::all(0); //black background
    cv::drawContours( mask, contours, -1, cv::Scalar::all(255)); //white contours

    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/paperContours.jpg", mask);
        cv::waitKey(500);
    }

    // find the biggest contour
    for (int index = 0; index < contours.size(); index++)  
    {  
        double tmparea = fabs(contourArea(contours[index]));  
        if (tmparea > maxarea)  
        {  
            maxarea = tmparea;  
            maxAreaIdx = index; //Record the index number of the maximum contour
        }  
    }

    //********************************************************************************************

    //find the paper rectangle
    paper_rotatedRect = cv::minAreaRect (contours[maxAreaIdx]);
    
    //get four corners of the rectangle
    paper_rotatedRect.points(rectVertex);

    //plot the rectangle
    for (int j = 0; j < 4; j++) 
    {
        cornersB[j].x = rectVertex[j].x;
        cornersB[j].y = rectVertex[j].y;
        //cv::line(colorImage, cornersB[j], cornersB[(j + 1) % 4], cv::Scalar::all(255), 2, 8);
        cv::circle(colorImage, cornersB[j], 5, cv::Scalar(0, 255, 0), 2, 8, 0); 
        if(writeImage)
        {
            cv::imwrite("/home/dongyi/Pictures/paperCorners.jpg", colorImage);
            cv::waitKey(500);
        }

        std::cout <<"the paper's cornerSubPix is "<<cornersB[j]<< '\n' << std::endl;
    }
    
    //find the grasp point
    //first find the center of the view
    centerx = (colorImage.size()).width / 2;
    centery = (colorImage.size()).height / 2;
    std::cout <<"the center of the view is "<<"("<<centerx<<", "<<centery<<")"<<std::endl;
    cv::Point2f center(centerx, centery);

    //second find the first corner point1
    cv::Point2f point1 = getClosestPoint (center, cornersB);
    std::cout <<"the paper's point1 is "<<point1<< std::endl;
    
    //third find the second corner point2
    cornersB.erase(remove(cornersB.begin(),cornersB.end(),cornersB[k]),cornersB.end()); //k is local variable
    //std::cout <<cornersA<< std::endl;
    cv::Point2f point2 = getClosestPoint (point1, cornersB);
    std::cout <<"the paper's point2 is "<<point2<< std::endl;

    //finally paper grasp point
    graspPoint.x = (point1.x + point2.x) / 2;
    graspPoint.y = (point1.y + point2.y) / 2;
    std::cout <<"the paper's grasppoint is "<<graspPoint<< std::endl;
    //cv::circle(colorImage, graspPoint, 5, cv::Scalar(0, 0, 255), 2, 8, 0);
    //cv::imshow("paper_corner_sub", colorImage); 
    //cv::waitKey(1000);
    
    //get the corresponding 3D coordinates
    float real_z = 0.001 * depthImage.at<u_int16_t>( graspPoint.y, graspPoint.x );    //x y who is the first!!!
    float real_x = (graspPoint.x - camera_info.K.at( 2 ) ) / camera_info.K.at( 0 ) * real_z;  //real_z is very important
    float real_y = (graspPoint.y - camera_info.K.at( 5 ) ) / camera_info.K.at( 4 ) * real_z;
    
    //print paper graspPoint
    char tam[100];
    sprintf( tam, "(%0.2f,%0.2f,%0.2f)", real_x, real_y, real_z );
    putText( colorImage, tam, graspPoint, cv::FONT_HERSHEY_SIMPLEX, 0.6, cvScalar( 0, 0, 255 ), 1 );//打印到屏幕上
    cv::circle(colorImage, graspPoint, 5, cv::Scalar(0, 0, 255), 2, 8, 0);
  
    //define paper grasp pose
    paper_graspPose.header.frame_id = "camera_color_frame";
    paper_graspPose.header.stamp = ros::Time();
    paper_graspPose.pose.position = coordinateMap( graspPoint ).point;

    r = 0;
    p = 0;
    y = atan ((point2.y - point1.y) / (point2.x - point1.x));
    std::cout <<"the paper's angle is "<< y << std::endl;
    paper_graspPose.pose.orientation = tf::createQuaternionMsgFromYaw( y ); //2D grasp pose
    //tf::createQuaternionMsgFromRollPitchYaw( - r, p, y); //Returns a quaternion

    //publish the paper grasp Pose
    paper_graspPose_pub_.publish(paper_graspPose);
    
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/paperGraspPose.jpg", colorImage);
        cv::waitKey(500);
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

//shoe pose estimation
void VisionManager::detectShoe(const sensor_msgs::ImageConstPtr &msg, float &pixel_x, float &pixel_y, cv::Rect &shoeBox)
{
    ROS_INFO_STREAM("Detecting shoe now...");

    cv::Mat g_hsv, mask, g_color_blur, g_binary;
    cv::RotatedRect boundRect;
    std::vector< std::vector< cv::Point > > contours;
    cv::Point2f rectVertex[4], graspPoint, detectPoint1, detectPoint2;
    std::vector < cv::Point2f > cornersB(4);
    double r, p, y;
    float yk;
    double maxarea = 0;  
    int maxAreaIdx = 0; 
	
    //get shoe binary image
    cv::blur(colorImage, g_color_blur, cv::Size(5,5));
    cv::cvtColor( g_color_blur, g_hsv, cv::COLOR_BGR2HSV);
    cv::Scalar scalarL = cv::Scalar(80, 43, 46);
    cv::Scalar scalarH = cv::Scalar(95, 255, 255);
    cv::inRange(g_hsv, scalarL, scalarH, mask); //the green background will become to white color
    mask = 255-mask; //black and white inversion
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/shoeMask.jpg", mask);
        cv::waitKey(500);
    }
    
    g_binary = mask(shoeBox);
    dilate(g_binary, g_binary, cv::Mat());

    //find the shoe contour
    cv::findContours
    (
        g_binary,
        contours,
        cv::noArray(),
        cv::RETR_EXTERNAL,
        cv::CHAIN_APPROX_SIMPLE
    );

    g_binary = cv::Scalar::all(0); //black background
    cv::drawContours( g_binary, contours, -1, cv::Scalar::all(255)); //white contours
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/shoeContours.jpg", g_binary);
        cv::waitKey(500);
    }

    //find the giggest contour
    for (int index = 0; index < contours.size(); index++)  
    {  
        double tmparea = fabs(cv::contourArea(contours[index]));  
        if (tmparea > maxarea)  
        {  
            maxarea = tmparea;  
            maxAreaIdx = index; //Record the index number of the maximum contour
        }  
    }

    //find the shoe rectangle
    boundRect = cv::minAreaRect (contours[maxAreaIdx]);
    //rectangle(g_binary, boundRect.tl(), boundRect.br(), cv::Scalar::all(255), 2, 8, 0);
    boundRect.points(rectVertex); //get the four corners of the rectangle
    
    //plot the shoe rectangle
    for (int i = 0; i < 4; i++)
    {
        cornersB[i].x = rectVertex[i].x + shoeBox.x;
        cornersB[i].y = rectVertex[i].y + shoeBox.y;
        cv::line(g_binary, rectVertex[i], rectVertex[(i + 1) % 4], cv::Scalar::all(255), 2, 8);
        std::cout <<"the shoe's cornerPix is "<< rectVertex[i] << '\n' << std::endl;
    }
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/shoeMinRect.jpg", g_binary);
        cv::waitKey(500);
    }

    //first find the center of the image
    float centerx = (colorImage.size()).width / 2;
    float centery = (colorImage.size()).height / 2;
    std::cout <<"the center of the colorImage is "<<"("<<centerx<<", "<<centerx<<")"<<std::endl;
    cv::Point2f center(centerx , centery);

    //second find the first corner point1
    cv::Point2f point1 = getClosestPoint (center, cornersB);
    std::cout <<"the shoe's point1 is "<<point1<< std::endl;

    //third find the second corner point2
    cornersB.erase(remove(cornersB.begin(),cornersB.end(),cornersB[k]),cornersB.end()); 
    //std::cout <<cornersA<< std::endl;
    cv::Point2f point2 = getClosestPoint (point1, cornersB);
    std::cout <<"the shoe's point2 is "<<point2<< std::endl;

    //slope of the shoe
    yk = (point2.y - point1.y) / (point2.x - point1.x);

    //get the points on the middle line
    detectPoint1.x = (point1.x + point2.x) / 2;
    detectPoint1.y = (point1.y + point2.y) / 2;
    cornersB.erase(remove(cornersB.begin(),cornersB.end(),cornersB[k]),cornersB.end()); 

    detectPoint2.x = (cornersB[0].x + cornersB[1].x) / 2;
    detectPoint2.y = (cornersB[0].y + cornersB[1].y) / 2;

    //get the highest point of the shoe
    float delta = getDistance(detectPoint1, detectPoint2);
    float depthMin = 100000;
    cv::Point2f pointMin;

    if(delta > 0)
    {
        for (int i = 0; i < delta; i++) 
        {
            cv::Point2f pointTemp;

            pointTemp.x = detectPoint1.x * i/delta + detectPoint2.x * (delta - i)/delta;
            pointTemp.y = detectPoint1.y * i/delta + detectPoint2.y * (delta - i)/delta;

            float depthTemp = coordinateMap(pointTemp).point.z;
            if (depthTemp < depthMin && depthTemp > 0.001)
            {
                depthMin = depthTemp;
                pointMin = pointTemp;
            }
        }
    }

    std::cout <<"the shoe's pointMin is "<<pointMin<< std::endl;
    cv::circle(colorImage, pointMin, 5, cv::Scalar(0, 0, 255), 2, 8, 0);

    //plot the center of the shoe rectangle
    graspPoint.x = boundRect.center.x + shoeBox.x;
    graspPoint.y = boundRect.center.y + shoeBox.y;
    std::cout <<"the shoe's center is "<<graspPoint<< std::endl;
    
    //get the corresponding 3D coordinates
    float real_z = 0.001 * depthImage.at<u_int16_t>( graspPoint.y, graspPoint.x );    //x y who is the first!!!
    float real_x = (graspPoint.x - camera_info.K.at( 2 ) ) / camera_info.K.at( 0 ) * real_z;  //real_z is very important
    float real_y = (graspPoint.y - camera_info.K.at( 5 ) ) / camera_info.K.at( 4 ) * real_z;
    
    //print the shoe grasp point
    char tam[100];
    sprintf( tam, "(%0.2f,%0.2f,%0.2f)", real_x, real_y, real_z );
    putText( colorImage, tam, graspPoint, cv::FONT_HERSHEY_SIMPLEX, 0.6, cvScalar( 0, 0, 255 ), 1 );//打印到屏幕上
    cv::circle(colorImage, graspPoint, 5, cv::Scalar(0, 0, 255), 2, 8, 0);
    
    //define shoe grasp pose
    shoe_graspPose.header.frame_id = "camera_color_frame";
    shoe_graspPose.header.stamp = ros::Time();
    shoe_graspPose.pose.position = coordinateMap( graspPoint ).point;

    //define the shoe orientation
    r = 0;
    p = 0;
    if(yk > 0)
    {
        if (pointMin.y > graspPoint.y) 
        {
            y = (boundRect.angle) * 3.14 / 180;
        }
        else if ((pointMin.y = graspPoint.y))
        {
            if (pointMin.x < graspPoint.x)
            {
                y = (boundRect.angle) * 3.14 / 180;
            }
            else
            {
                y = (boundRect.angle - 180) * 3.14 / 180;
            }
        }
        else
        {
            y = (boundRect.angle - 180) * 3.14 / 180;
        }
    }
    else
    {
        if (pointMin.y > graspPoint.y) 
        {
            y = (boundRect.angle - 90) * 3.14 / 180;;
        }
        else
        {
            y = (boundRect.angle - 90 - 180) * 3.14 / 180;
        }
    }
    std::cout <<"the shoe's angle is "<< y << std::endl;

    //get shoe grasp pose and publish
    shoe_graspPose.pose.orientation = tf::createQuaternionMsgFromYaw( y );
    shoe_graspPose_pub_.publish(shoe_graspPose);

    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/shoPose.jpg", colorImage);
        cv::waitKey(500);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//box pose estimation
void VisionManager::detectBox(const sensor_msgs::ImageConstPtr &msg, float &pixel_x, float &pixel_y, cv::Rect &boxBox)
{
    std::vector < cv::Point2f > cornersA(10), cornersB(4);
    cv::Point2f grasppoint1, grasppoint2, grasppoint3, grasppoint1_temp, grasppoint2_temp, grasppoint3_temp, box_rectCenter13, box_rectCenter23;
    cv::Point2f rectVertex[4], rotatedRectCenter13, rotatedRectCenter23;
    cv::Mat denoiseImage1, denoiseImage2, boxHsv, mask;
    cv::Rect boxRect2;
    cv::RotatedRect box_rotatedRect; 
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Point> polyContours;

    float centerx, centery;
    double r, p, y1, y2, y3;
    double maxarea = 0; 
    int maxAreaIdx = 0; 

    //********************************************************************************

    //get two points in the box area
    box_rectCenter13.x = boxBox.x + boxBox.width / 3;
    box_rectCenter13.y = boxBox.y + boxBox.height / 2;
    box_rectCenter23.x = boxBox.x + boxBox.width * 2 / 3;
    box_rectCenter23.y = boxBox.y + boxBox.height / 2;
    
    cv::medianBlur(colorImage, denoiseImage1, 3);
    denoiseImage2 = denoiseImage1.clone();
    cv::cvtColor( denoiseImage2, boxHsv, cv::COLOR_BGR2HSV);
    
    //box segmentation
    cv::floodFill
    (
        boxHsv, 
        box_rectCenter23, 
        cv::Scalar(1, 255, 255), 
        &boxRect2, 
        cv::Scalar(3, 100, 100), 
        cv::Scalar(3, 100, 100)
    );
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/boxSegmentation.jpg", boxHsv);
        cv::waitKey(500);
    }
    
    //********************************************************************
    
    //get box bianry image
    cv::Scalar scalarL = cv::Scalar(0, 255, 255); 
    cv::Scalar scalarH = cv::Scalar(1, 255, 255);
    cv::inRange(boxHsv, scalarL, scalarH, mask);
    dilate(mask, mask, cv::Mat());

    //find the box contour
    cv::findContours
    (
        mask,
        contours,
        cv::noArray(),
        cv::RETR_EXTERNAL,
        cv::CHAIN_APPROX_SIMPLE
    );

    mask = cv::Scalar::all(0); //black background
    cv::drawContours( mask, contours, -1, cv::Scalar::all(255) ); //white contours
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/boxContours.jpg", mask);
        cv::waitKey(500);
    }
    
    // find the biggest contour
    for (int index = 0; index < contours.size(); index++)  
    {  
        double tmparea = fabs(cv::contourArea(contours[index]));  
        if (tmparea > maxarea)  
        {  
            maxarea = tmparea;  
            maxAreaIdx = index;
        }  
    }
    std::cout <<"the biggestContour's length is "<< contours[maxAreaIdx].size() << '\n' << std::endl;

    //get box convex hull
    cv::approxPolyDP(contours[maxAreaIdx], polyContours, 10, true);
    for (int i = 0; i < polyContours.size(); ++i)
    {
        cv::line(mask, polyContours[i], polyContours[(i + 1) % polyContours.size()], cv::Scalar::all(255), 2, 8);
        //cv::circle(mask, polyContours[i], 1, cv::Scalar(0, 255, 0), 2, 8, 0);
    }
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/boxPolyContours.jpg", mask);
        cv::waitKey(500);
    }
    
    std::vector<int> hull;
    convexHull(polyContours, hull, false);
    
    for (int i = 0; i < hull.size(); ++i)
    {
        cornersA[i] = polyContours[hull[i]];
        //cv::circle(colorImage, polyContours[hull[i]], 5, cv::Scalar(0, 255, 0), 2, 8, 0);
        
    }
    
    //**************************************************************************************
    
    //find the box rectangle
    box_rotatedRect = cv::minAreaRect (contours[maxAreaIdx]); 
    
    //get box right angle corners
    box_rotatedRect.points(rectVertex);
    for (int i = 0; i < 4; ++i)
    {
        cv::line(mask, rectVertex[i], rectVertex[(i + 1) % 4], cv::Scalar::all(255), 2, 8);
       
        cornersB[i] = getClosestPoint(rectVertex[i], cornersA);
        cv::circle(colorImage, cornersB[i], 5, cv::Scalar(0, 255, 0), 2, 8, 0);
    }
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/boxMinRect.jpg", mask);
        cv::waitKey(500);
    }
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/boxCorners.jpg", colorImage);
        cv::waitKey(500);
    }
 
    //***********************************************************************************************

    //find the grasp point

    //first find the center
    centerx = (colorImage.size()).width / 2;
    centery = (colorImage.size()).height / 2;
    std::cout <<"the center of the boxImage is "<<"("<<centerx<<", "<<centery<<")"<<std::endl;
    cv::Point2f center(centerx, centery);

    //second find the first corner point1
    cv::Point2f point1 = getClosestPoint (center, cornersB);
    std::cout <<"the box's point1 is "<<point1<< std::endl;
    
    //third find the second corner point2
    cornersB.erase(remove(cornersB.begin(),cornersB.end(),cornersB[k]),cornersB.end()); 
    cv::Point2f point2 = getClosestPoint (point1, cornersB);
    std::cout <<"the box's point2 is "<<point2<< std::endl;

    //get tenporary grasp point1
    grasppoint1_temp.x = (point1.x + point2.x) / 2;
    grasppoint1_temp.y = (point1.y + point2.y) / 2;
    std::cout <<"the box's grasppoint1_temp is "<<grasppoint1_temp<< std::endl;
    //cv::circle(colorImage, grasppoint1_temp, 5, cv::Scalar(0, 0, 255), 2, 8, 0);

    //***************************************************************************************

    //get tenporary grasp point2
    cornersB.erase(remove(cornersB.begin(),cornersB.end(),cornersB[k]),cornersB.end()); 
    grasppoint2_temp.x = (cornersB[0].x + cornersB[1].x) / 2;
    grasppoint2_temp.y = (cornersB[0].y + cornersB[1].y) / 2;
    std::cout <<"the box's grasppoint2_temp is "<<grasppoint2_temp<< std::endl;
    //cv::circle(colorImage, grasppoint2_temp, 5, cv::Scalar(0, 0, 255), 2, 8, 0);

    //real grasp point1 belongs to the box cover
    rotatedRectCenter13.x = grasppoint1_temp.x * 2/3 + grasppoint2_temp.x * 1/3;
    rotatedRectCenter13.y = grasppoint1_temp.y * 2/3 + grasppoint2_temp.y * 1/3;
    rotatedRectCenter23.x = grasppoint1_temp.x * 1/3 + grasppoint2_temp.x * 2/3;
    rotatedRectCenter23.y = grasppoint1_temp.y * 1/3 + grasppoint2_temp.y * 2/3;
    
    if (coordinateMap(rotatedRectCenter13).point.z < coordinateMap(rotatedRectCenter23).point.z)
    {
        grasppoint1 = grasppoint1_temp;
        grasppoint2 = grasppoint2_temp;
    }
    else
    {
        grasppoint1 = grasppoint2_temp;
        grasppoint2 = grasppoint1_temp;
    }

    cv::Point2f pointMin, real_grasppoint1, real_grasppoint2;
    real_grasppoint1 = grasppoint1;
    real_grasppoint2 = grasppoint2;

    //obtain a robust depth data 
    for (int i = 480; i <= 520; i++) 
    {
        pointMin.x = grasppoint1.x * i/500 + grasppoint2.x * (500 - i)/500;
        pointMin.y = grasppoint1.y * i/500 + grasppoint2.y * (500 - i)/500;
        
        if (coordinateMap(pointMin).point.z < coordinateMap( real_grasppoint1 ).point.z && fabs(coordinateMap(pointMin).point.z) > 0.001)
        {
            real_grasppoint1 = pointMin;
        }
    }

    for (int i = -20; i <= 20; i++) 
    {
        pointMin.x = grasppoint1.x * i/500 + grasppoint2.x * (500 - i)/500;
        pointMin.y = grasppoint1.y * i/500 + grasppoint2.y * (500 - i)/500;
        
        if (coordinateMap(pointMin).point.z < coordinateMap( real_grasppoint2 ).point.z && fabs(coordinateMap(pointMin).point.z) > 0.001)
        {
            real_grasppoint2 = pointMin;
        }
    }

    //plot grasp point1 and grasp point2
    grasppoint1 = real_grasppoint1;
    grasppoint2 = real_grasppoint2;
    cv::circle(colorImage, grasppoint1, 5, cv::Scalar(0, 0, 255), 2, 8, 0);
    cv::circle(colorImage, grasppoint2, 5, cv::Scalar(0, 0, 255), 2, 8, 0);
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/boxGraspPoint12.jpg", colorImage);
        cv::waitKey(500);
    }
    
    //***************************************************************************************

    //get grasp point3
    float depthMin = 10000;

    for (int i = 100; i < 400; i++) 
    {
        pointMin.x = grasppoint1.x * i/500 + grasppoint2.x * (500 - i)/500;
        pointMin.y = grasppoint1.y * i/500 + grasppoint2.y * (500 - i)/500;
        
        if (coordinateMap(pointMin).point.z < depthMin && fabs(coordinateMap(pointMin).point.z) > 0.001)
        {
            depthMin = coordinateMap(pointMin).point.z;
            grasppoint3 = pointMin;
        }
    }

    std::cout <<"the box's grasppoint3 is "<<grasppoint3<< std::endl;

    cv::circle(colorImage, grasppoint3, 5, cv::Scalar(0, 0, 255), 2, 8, 0);
    if(writeImage)
    {
        cv::imwrite("/home/dongyi/Pictures/boxGraspPoint123.jpg", colorImage);
        cv::waitKey(500);
    }

    //***************************************************************************************
    
    //get the corresponding 3D coordinates
    //print grasp point3
    float real_z = 0.001 * depthImage.at<u_int16_t>( grasppoint3.y, grasppoint3.x );    //x y who is the first!!!
    float real_x = (grasppoint3.x - camera_info.K.at( 2 ) ) / camera_info.K.at( 0 ) * real_z;  //real_z is very important
    float real_y = (grasppoint3.y - camera_info.K.at( 5 ) ) / camera_info.K.at( 4 ) * real_z;
    char tam[100];
    sprintf( tam, "(%0.3f, %0.3f, %0.3f)", real_x, real_y, real_z );
    //putText( colorImage, tam, grasppoint3, cv::FONT_HERSHEY_SIMPLEX, 0.6, cvScalar( 0, 0, 255 ), 1 );

    //print grasp point2
    real_z = 0.001 * depthImage.at<u_int16_t>( grasppoint2.y, grasppoint2.x );    //x y who is the first!!!
    real_x = (grasppoint2.x - camera_info.K.at( 2 ) ) / camera_info.K.at( 0 ) * real_z;  //real_z is very important
    real_y = (grasppoint2.y - camera_info.K.at( 5 ) ) / camera_info.K.at( 4 ) * real_z;
    sprintf( tam, "(%0.3f, %0.3f, %0.3f)", real_x, real_y, real_z );
    putText( colorImage, tam, grasppoint2, cv::FONT_HERSHEY_SIMPLEX, 0.6, cvScalar( 0, 0, 255 ), 1 );

    //print grasp point1
    real_z = 0.001 * depthImage.at<u_int16_t>( grasppoint1.y, grasppoint1.x );    
    real_x = (grasppoint1.x - camera_info.K.at( 2 ) ) / camera_info.K.at( 0 ) * real_z;  
    real_y = (grasppoint1.y - camera_info.K.at( 5 ) ) / camera_info.K.at( 4 ) * real_z;
    sprintf( tam, "(%0.3f, %0.3f, %0.3f)", real_x, real_y, real_z );
    putText( colorImage, tam, grasppoint1, cv::FONT_HERSHEY_SIMPLEX, 0.6, cvScalar( 0, 0, 255 ), 1 );

    //***************************************************************************************
    
    box_graspPose1.position = coordinateMap( grasppoint1 ).point;
    box_graspPose2.position = coordinateMap( grasppoint2 ).point;
    box_graspPose3.position = coordinateMap( grasppoint3 ).point;

    //define grasp pose
    r = 0;
    p = 0;
    y1 = y2 = y3 = atan ((point2.y - point1.y) / (point2.x - point1.x));

    box_graspPose1.orientation = tf::createQuaternionMsgFromYaw( y1 );
    box_graspPose2.orientation = tf::createQuaternionMsgFromYaw( y2 );
    box_graspPose3.orientation = tf::createQuaternionMsgFromYaw( - y3 );
    //tf::createQuaternionMsgFromRollPitchYaw( - r, p, y);

    box_graspPose.header.frame_id = "camera_color_frame";
    box_graspPose.header.stamp = ros::Time();
    box_graspPose.poses.push_back(box_graspPose1);
    box_graspPose.poses.push_back(box_graspPose2);
    box_graspPose.poses.push_back(box_graspPose3);

    box_graspPose_pub_.publish(box_graspPose);

    imshow("box_corner_sub", colorImage); 
    cv::waitKey(500);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//*****************************************help function********************************************//
////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////

//get distance between two points
float VisionManager::getDistance(cv::Point2f pointA, cv::Point2f pointB )
{
    float distance;
    distance = powf((pointA.x - pointB.x),2) + powf((pointA.y - pointB.y),2);
    distance = sqrtf(distance); 

    return distance;
}

//get the closest point between the pointC and corners (points)
cv::Point2f VisionManager::getClosestPoint(cv::Point2f pointC, std::vector < cv::Point2f > corners )
{
    int k_temp = 0;
	float mindistance, realdistance;
    cv::Point2f realPoint;

    mindistance = getDistance(pointC, corners[0]);

    for (int j = 0; j < corners.size(); j++) 
    {
		realdistance = getDistance(pointC, corners[j]);

        if (mindistance > realdistance)
        {
            mindistance = realdistance;
            k_temp = j;
        }
    }

    realPoint = corners[k_temp];
    k = k_temp;
    return realPoint;
}

///////////////////////////////////////////////////////////////////////////////////////////////

//get cameraInfo
void VisionManager::cameraInfoCb( const sensor_msgs::CameraInfo &msg )
{
    camera_info = msg;
}

//get depth image
void VisionManager::imageDepthCb( const sensor_msgs::ImageConstPtr &msg )
{
	cv_bridge::CvImagePtr cv_ptr;

	try 
    {
		cv_ptr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::TYPE_16UC1 );
		depthImage = cv_ptr->image;
	} 
    catch ( cv_bridge::Exception &e ) 
    {
		ROS_ERROR( "cv_bridge exception: %s", e.what() );
		return;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

//pixel coordinate to camera coordinate

geometry_msgs::PointStamped VisionManager::coordinateMap(cv::Point2f graspPoint)
{
    geometry_msgs::PointStamped	outputPoint;
    
    float real_z = 0.001 * depthImage.at<u_int16_t>( graspPoint.y, graspPoint.x );    //x y who is the first!!!
    float real_x = (graspPoint.x - camera_info.K.at( 2 ) ) / camera_info.K.at( 0 ) * real_z;  //real_z is very important
    float real_y = (graspPoint.y - camera_info.K.at( 5 ) ) / camera_info.K.at( 4 ) * real_z;
    
    outputPoint.header.frame_id = "/camera_depth_optical_frame";
    outputPoint.header.stamp = ros::Time::now();
    outputPoint.point.x = real_x;
    outputPoint.point.y = real_y;
    outputPoint.point.z = real_z;
    return outputPoint;
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////

//Find the sharp corners of the contour

std::vector <cv::Point2f>  VisionManager::contourCorner(std::vector<std::vector<cv::Point>> &contours)
{
    float fmax = -1; 
    int   imax = -1;
    bool  bstart = false;
    int j = 0;
    std::vector <cv::Point2f> cornersA(100);
    double maxarea = 0;  
    int maxAreaIdx = 0; 

    // finding the biggest contour
    for (int index = 0; index < contours.size(); index++)  
    {  
        double tmparea = fabs(cv::contourArea(contours[index]));  
        if (tmparea > maxarea)  
        {  
            maxarea = tmparea;  
            maxAreaIdx = index;
        }  
    }

    int icount = contours[maxAreaIdx].size();
    for (int i=0; i < contours[maxAreaIdx].size(); i++)
    {
        cv::Point2f pa = contours[maxAreaIdx][(i + icount - 30) % icount];
        cv::Point2f pb = contours[maxAreaIdx][(i + icount + 30) % icount];
        cv::Point2f pc = contours[maxAreaIdx][i];

        float fa = getDistance(pa, pb);
        float fb = getDistance(pa, pc) + getDistance(pb, pc);
        float fang = fa / fb;
        float fsharp = 1 - fang;

        if (fsharp>0.1)
        {
            bstart = true;
            if (fsharp>fmax)
            {
                fmax = fsharp;
                imax = i;
            }
        }
        else
        {
            if (bstart)
            {
                cornersA[j] = contours[maxAreaIdx][imax];
                imax  = -1;
                fmax  = -1;
                bstart = false;
                j = j + 1;
            }
        }
    }  

    return cornersA;
}

/*
void VisionManager::getRotatedRect(cv::Mat image, cv::RotatedRect rotatedRect)
{
    cv::Point2f vertices[4];
    rotatedRect.points(vertices);
    
    cv::Point2f center = rotatedRect.center;
    cv::Mat rot_mat = getRotationMatrix2D(center, rotatedRect.angle, 1.0);
    cv::Size dst_sz(image.size());
    cv::warpAffine(image, image, rot_mat, dst_sz);
}*/

////////////////////////////////////////////////////////////////////////
//*************************main function******************************//
////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv ) 
{
    ros::init(argc, argv, "simple_grasping_vision_detection");
    ros::NodeHandle n_;
    
    ROS_INFO_STREAM("Waiting for two seconds..");
    ros::WallDuration(2.0).sleep();
    
    VisionManager vm(n_);
    while (ros::ok())
    {
        //image callback
        ros::spinOnce();
        os::WallDuration(2.0).sleep();
    }
    
    return 0;
}
