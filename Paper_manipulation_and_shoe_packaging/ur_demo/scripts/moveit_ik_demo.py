#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Author: Dong Yi
#Contact: dongyi@nuaa.edu.cn
#Update: 22/02/2022

import rospy, sys
import moveit_commander
from geometry_msgs.msg import PoseStamped, Pose


class MoveItIkDemo:
    def __init__(self):
        #Initialize move_group API
        moveit_commander.roscpp_initialize(sys.argv)
        
        #Initialize ROS node
        rospy.init_node('moveit_ik_demo')
                
        #Initialize arm group
        arm = moveit_commander.MoveGroupCommander('manipulator')
                
        #Get the name of the end link
        end_effector_link = arm.get_end_effector_link()
                        
        #Set the reference frame
        reference_frame = 'base_link'
        arm.set_pose_reference_frame(reference_frame)
                
        #Allowable replanning
        arm.allow_replanning(True)
        
        #Allowable error
        arm.set_goal_position_tolerance(0.001)
        arm.set_goal_orientation_tolerance(0.01)
       
        #Allowable maximum speed and acceleration
        arm.set_max_acceleration_scaling_factor(0.5)
        arm.set_max_velocity_scaling_factor(0.5)

        #Go backb to home position
        arm.set_named_target('home')
        arm.go()
        rospy.sleep(1)
               
        #Set the target pose
        target_pose = PoseStamped()
        target_pose.header.frame_id = reference_frame
        target_pose.header.stamp = rospy.Time.now()     
        target_pose.pose.position.x = 0.2593
        target_pose.pose.position.y = 0.0636
        target_pose.pose.position.z = 0.1787
        target_pose.pose.orientation.x = 0.70692
        target_pose.pose.orientation.y = 0.0
        target_pose.pose.orientation.z = 0.0
        target_pose.pose.orientation.w = 0.70729
        
        #Get current pose
        arm.set_start_state_to_current_state()
        
        #Set taget pose
        arm.set_pose_target(target_pose, end_effector_link)
        
        #Path planning
        traj = arm.plan()
        
        #Excute
        arm.execute(traj)
        rospy.sleep(1)

        #Go back to home pose
        arm.set_named_target('home')
        arm.go()

        moveit_commander.roscpp_shutdown()
        moveit_commander.os._exit(0)

if __name__ == "__main__":
    MoveItIkDemo()

    
    
