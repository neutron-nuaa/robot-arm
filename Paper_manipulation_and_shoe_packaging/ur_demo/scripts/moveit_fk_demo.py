#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Author: Dong Yi
#Contact: dongyi@nuaa.edu.cn
#Update: 22/02/2022

import rospy, sys
import moveit_commander

class MoveItFkDemo:
    def __init__(self):
        #Initialize move_group API
        moveit_commander.roscpp_initialize(sys.argv)

        #Initialize ROS node
        rospy.init_node('moveit_fk_demo', anonymous=True)
 
        #Initialize arm group
        arm = moveit_commander.MoveGroupCommander('manipulator')
        
        #Allowable error
        arm.set_goal_joint_tolerance(0.001)

        #Allowable maximum speed and acceleration
        arm.set_max_acceleration_scaling_factor(0.5)
        arm.set_max_velocity_scaling_factor(0.5)
        
        #Go back to home position
        arm.set_named_target('home')
        arm.go()
        rospy.sleep(1)
         
        #Set the target pose
        joint_positions = [0.391410, -0.676384, -0.376217, 0.0, 1.052834, 0.454125]
        arm.set_joint_value_target(joint_positions)
                 
        #Excute
        arm.go()
        rospy.sleep(1)

        #Go back to home position
        arm.set_named_target('home')
        arm.go()
        rospy.sleep(1)
        
        moveit_commander.roscpp_shutdown()
        moveit_commander.os._exit(0)

if __name__ == "__main__":
    try:
        MoveItFkDemo()
    except rospy.ROSInterruptException:
        pass
