#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Author: Dong Yi
#Contact: dongyi@nuaa.edu.cn
#Update: 22/02/2022
 
import rospy, sys
import thread, copy
import moveit_commander
from moveit_commander import RobotCommander, MoveGroupCommander, PlanningSceneInterface
from geometry_msgs.msg import PoseStamped, Pose
from moveit_msgs.msg import CollisionObject, AttachedCollisionObject, PlanningScene
from math import radians
from copy import deepcopy

class MoveAttachedObjectDemo:
    def __init__(self):
        #Initialize move_group API
        moveit_commander.roscpp_initialize(sys.argv)
        
        #Initialize ROS node
        rospy.init_node('moveit_attached_object_demo')
        
        #Initialize scene objects
        scene = PlanningSceneInterface()
        rospy.sleep(1)
                                
        #Initialize arm group
        arm = MoveGroupCommander('manipulator')
        
        #Get the name of the end link
        end_effector_link = arm.get_end_effector_link()
        
        #Allowable error
        arm.set_goal_position_tolerance(0.01)
        arm.set_goal_orientation_tolerance(0.05)
       
        #Allowable replanning
        arm.allow_replanning(True)
        arm.set_planning_time(10)

        #Go back to home position
        arm.set_named_target('home')
        arm.go()
        
        #Remove objects left
        scene.remove_attached_object(end_effector_link, 'tool')
        scene.remove_world_object('table') 
        scene.remove_world_object('target')

        #Set the height of the desktop
        table_ground = 0.6
        
        #Set the size of the table and tool 
        table_size = [0.1, 0.7, 0.01]
        tool_size = [0.2, 0.02, 0.02]
        
        #Set the pose of the tool
        p = PoseStamped()
        p.header.frame_id = end_effector_link
        
        p.pose.position.x = tool_size[0] / 2.0 - 0.025
        p.pose.position.y = -0.015
        p.pose.position.z = 0.0
        p.pose.orientation.x = 0
        p.pose.orientation.y = 0
        p.pose.orientation.z = 0
        p.pose.orientation.w = 1
        
        #Attach the tool to the end of the robot
        scene.attach_box(end_effector_link, 'tool', p, tool_size)

        #Add table to the scene
        table_pose = PoseStamped()
        table_pose.header.frame_id = 'base_link'
        table_pose.pose.position.x = 0.25
        table_pose.pose.position.y = 0.0
        table_pose.pose.position.z = table_ground + table_size[2] / 2.0
        table_pose.pose.orientation.w = 1.0
        scene.add_box('table', table_pose, table_size)
        
        rospy.sleep(2)  

        #Update the current pose
        arm.set_start_state_to_current_state()

        #Set the target position
        joint_positions = [0.827228546495185, 0.29496592875743577, 1.1185644936946095, -0.7987583317769674, -0.18950024740190782, 0.11752152218233858]
        arm.set_joint_value_target(joint_positions)
                 
        #Excute
        arm.go()
        rospy.sleep(1)
        
        #Go back to home position
        arm.set_named_target('home')
        arm.go()

        moveit_commander.roscpp_shutdown()
        moveit_commander.os._exit(0)

if __name__ == "__main__":
    MoveAttachedObjectDemo()

    
