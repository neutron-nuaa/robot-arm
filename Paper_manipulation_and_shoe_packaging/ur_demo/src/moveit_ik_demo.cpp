/***********************************************************************
Author: Dong Yi
Contact: dongyi@nuaa.edu.cn
Update: 22/02/2022
***********************************************************************/

#include <string>
#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "moveit_fk_demo");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    moveit::planning_interface::MoveGroupInterface arm("manipulator");

    //Get the name of the end link
    std::string end_effector_link = arm.getEndEffectorLink();

    //Set the reference frame
    std::string reference_frame = "base_link";
    arm.setPoseReferenceFrame(reference_frame);

    //Allowable replanning
    arm.allowReplanning(true);

    //Allowable error
    arm.setGoalPositionTolerance(0.001);
    arm.setGoalOrientationTolerance(0.01);

    //Allowable maximum speed and acceleration
    arm.setMaxAccelerationScalingFactor(0.2);
    arm.setMaxVelocityScalingFactor(0.2);

    //Go back to home position
    //arm.setNamedTarget("home");
    //arm.move();
    //sleep(1);

    //Set the target pose
    geometry_msgs::Pose target_pose;
    target_pose.orientation.x = 0.70692;
    target_pose.orientation.y = 0.0;
    target_pose.orientation.z = 0.0;
    target_pose.orientation.w = 0.70729;

    target_pose.position.x = 0.2593;
    target_pose.position.y = 0.0636;
    target_pose.position.z = 0.1787;

    //Get current pose
    arm.setStartStateToCurrentState();

    arm.setPoseTarget(target_pose);

    //Path planning
    moveit::planning_interface::MoveGroupInterface::Plan plan;
    moveit::planning_interface::MoveItErrorCode success = arm.plan(plan);

    ROS_INFO("Plan (pose goal) %s",success?"":"FAILED");   

    //Excute
    if(success)
      arm.execute(plan);
    sleep(1);

    //Go back to home pose
    //arm.setNamedTarget("home");
    //arm.move();
    //sleep(1);

    ros::shutdown(); 

    return 0;
}
