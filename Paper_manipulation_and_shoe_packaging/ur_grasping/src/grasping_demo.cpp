/***********************************************************************
Author: Dong Yi
Contact: dongyi@nuaa.edu.cn
Update: 22/02/2022
***********************************************************************/

#include "ur_grasping/grasping_demo.h"

//constructor function
//subscribe to the detected objects
GraspingDemo::GraspingDemo(ros::NodeHandle n_) : 
it_(n_), 
armgroup("manipulator")
{
  this->nh_ = n_;
  grasp_running = false;
  
  this->pregrasp_x = pregrasp_x;
  this->pregrasp_y = pregrasp_y;
  this->pregrasp_z = pregrasp_z;

  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::WallDuration(5.0).sleep();
  ROS_INFO_STREAM("Go home...");
  attainPosition(pregrasp_x, pregrasp_y, pregrasp_z);

  std::cout<< " sin(30) :" << sin(30) << std::endl;
  std::cout<< " sin(30) :" << sin(30*3.1415/180) << std::endl;

  paper_graspPose_sub_ = n_.subscribe( "/paper_graspPose", 1, &GraspingDemo::paper_imageCb, this );
  shoe_graspPose_sub_ = n_.subscribe( "/shoe_graspPose", 1, &GraspingDemo::shoe_imageCb, this );
  box_graspPose_sub_ = n_.subscribe( "/box_graspPose", 1, &GraspingDemo::box_imageCb, this );

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// get objects' grasp poses

//transform the paper grasp pose from the camera's frame to the robot's frame
void GraspingDemo::paper_imageCb(const geometry_msgs::PoseStamped &pt)
{

  if (!grasp_running)
  {
    
    ROS_INFO_STREAM("Processing the Image to locate the paper Object...");

    try
    {
      listener.transformPose("base_link", pt, paper_graspPose); //Transform a Stamped Point Message into the target frame

      ROS_INFO("camera_color_frame:(%.2f, %.2f, %.2f) -----> base_link:(%.2f, %.2f, %.2f) at time %.2f", \
      pt.pose.position.x, pt.pose.position.y, pt.pose.position.z, \
      paper_graspPose.pose.position.x, paper_graspPose.pose.position.y, paper_graspPose.pose.position.z, \
      paper_graspPose.header.stamp.toSec());
    }
    catch(tf::TransformException &ex) 
    {
      ROS_ERROR("Received an exception trying to transform a pose form \"camera_color_frame\" to \"base_link\": %s", ex.what());
    }

    //Temporary Debug
    std::cout<< " X-position of paper_graspPoint in Robot Frame :" << paper_graspPose.pose.position.x << std::endl;
    std::cout<< " Y-position of paper_graspPoint in Robot Frame :" << paper_graspPose.pose.position.y << std::endl;
    std::cout<< " Z-position of paper_graspPoint in Robot Frame :" << paper_graspPose.pose.position.z << std::endl;

    std::cout<< " X-orientation of paper_graspPoint in Robot Frame :" << paper_graspPose.pose.orientation.x << std::endl;
    std::cout<< " Y-orientation of paper_graspPoint in Robot Frame :" << paper_graspPose.pose.orientation.y << std::endl;
    std::cout<< " Z-orientation of paper_graspPoint in Robot Frame :" << paper_graspPose.pose.orientation.z << std::endl;
    std::cout<< " w-orientation of paper_graspPoint in Robot Frame :" << paper_graspPose.pose.orientation.w << std::endl;
  }
}

//transform the shoe grasp pose from the camera's frame to the robot's frame
void GraspingDemo::shoe_imageCb(const geometry_msgs::PoseStamped &pt)
{

  if (!grasp_running)
  {
    
    ROS_INFO_STREAM("Processing the Image to locate the shoe Object...");

    try
    {
      listener.transformPose("base_link", pt, shoe_graspPose); //Transform a Stamped Point Message into the target frame

      ROS_INFO("camera_color_frame:(%.2f, %.2f, %.2f) -----> base_link:(%.2f, %.2f, %.2f) at time %.2f", \
      pt.pose.position.x, pt.pose.position.y, pt.pose.position.z, \
      shoe_graspPose.pose.position.x, shoe_graspPose.pose.position.y, shoe_graspPose.pose.position.z, \
      shoe_graspPose.header.stamp.toSec());
    }
    catch(tf::TransformException &ex) 
    {
      ROS_ERROR("Received an exception trying to transform a pose form \"camera_color_frame\" to \"base_link\": %s", ex.what());
    }

    // Temporary Debug
    std::cout<< " X-Co-ordinate of shoe_graspPoint in Robot Frame :" << shoe_graspPose.pose.position.x << std::endl;
    std::cout<< " Y-Co-ordinate of shoe_graspPoint in Robot Frame :" << shoe_graspPose.pose.position.y << std::endl;
    std::cout<< " Z-Co-ordinate of shoe_graspPoint in Robot Frame :" << shoe_graspPose.pose.position.z << std::endl;
  }
}

//transform the box grasp poses from the camera's frame to the robot's frame
void GraspingDemo::box_imageCb(const geometry_msgs::PoseArray &pt)
{
  geometry_msgs::PoseStamped pt1, pt2, pt3;

  if (!grasp_running)
  {
    
    ROS_INFO_STREAM("Processing the Image to locate the box Object...");

    pt1.pose = pt.poses[0];
    pt1.header.frame_id = pt.header.frame_id;
    pt1.header.stamp = pt.header.stamp;

    pt2.pose = pt.poses[1];
    pt2.header.frame_id = pt.header.frame_id;
    pt2.header.stamp = pt.header.stamp;

    pt3.pose = pt.poses[2];
    pt3.header.frame_id = pt.header.frame_id;
    pt3.header.stamp = pt.header.stamp;

    try
    {
      listener.transformPose("base_link", pt1, box_graspPose1); //Transform a Stamped Point Message into the target frame

      ROS_INFO("camera_color_frame:(%.2f, %.2f, %.2f) -----> base_link:(%.2f, %.2f, %.2f) at time %.2f", \
      pt1.pose.position.x, pt1.pose.position.y, pt1.pose.position.z, \
      box_graspPose1.pose.position.x, box_graspPose1.pose.position.y, box_graspPose1.pose.position.z, \
      box_graspPose1.header.stamp.toSec());
    }
    catch(tf::TransformException &ex) 
    {
      ROS_ERROR("Received an exception trying to transform a pose form \"camera_color_frame\" to \"base_link\": %s", ex.what());
    }

    try
    {
      listener.transformPose("base_link", pt2, box_graspPose2); //Transform a Stamped Point Message into the target frame

      ROS_INFO("camera_color_frame:(%.2f, %.2f, %.2f) -----> base_link:(%.2f, %.2f, %.2f) at time %.2f", \
      pt2.pose.position.x, pt2.pose.position.y, pt2.pose.position.z, \
      box_graspPose2.pose.position.x, box_graspPose2.pose.position.y, box_graspPose2.pose.position.z, \
      box_graspPose2.header.stamp.toSec());
    }
    catch(tf::TransformException &ex) 
    {
      ROS_ERROR("Received an exception trying to transform a pose form \"camera_color_frame\" to \"base_link\": %s", ex.what());
    }

    try
    {
      listener.transformPose("base_link", pt3, box_graspPose3); //Transform a Stamped Point Message into the target frame

      ROS_INFO("camera_color_frame:(%.2f, %.2f, %.2f) -----> base_link:(%.2f, %.2f, %.2f) at time %.2f", \
      pt3.pose.position.x, pt3.pose.position.y, pt3.pose.position.z, \
      box_graspPose3.pose.position.x, box_graspPose3.pose.position.y, box_graspPose3.pose.position.z, \
      box_graspPose3.header.stamp.toSec());
    }
    catch(tf::TransformException &ex) 
    {
      ROS_ERROR("Received an exception trying to transform a pose form \"camera_color_frame\" to \"base_link\": %s", ex.what());
    }

    grasp_running = true;

    //Temporary Debugging
    //std::cout<< " X-Co-ordinate of shoe_graspPoint in Robot Frame :" << box_graspPose.pose.position.x << std::endl;
    //std::cout<< " Y-Co-ordinate of shoe_graspPoint in Robot Frame :" << box_graspPose.pose.position.y << std::endl;
    //std::cout<< " Z-Co-ordinate of shoe_graspPoint in Robot Frame :" << box_graspPose.pose.position.z << std::endl;
    //std::cout<< " obj_camera_frame :" << obj_camera_frame << std::endl;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//help function

//straight path function
void GraspingDemo::straightPath(std::vector<geometry_msgs::Pose> waypoints)
{
  moveit_msgs::RobotTrajectory trajectory;
  const double jump_threshold = 0.0;
  const double eef_step = 0.01;
  double fraction = 0.0;
  int maxtries = 100;   //Maximum planning attempts
  int attempts = 0;     //Number of attempts to plan

  //get the name of the end link
  std::string end_effector_link = armgroup.getEndEffectorLink();

  //set the reference coordinate
  std::string reference_frame = "base_link";
  armgroup.setPoseReferenceFrame(reference_frame);

  //replanning is allowed
  armgroup.allowReplanning(true);

  //allowable error
  armgroup.setGoalPositionTolerance(0.001);
  armgroup.setGoalOrientationTolerance(0.01);

  //allowable maximum speed and acceleration
  armgroup.setMaxAccelerationScalingFactor(0.1);
  armgroup.setMaxVelocityScalingFactor(0.1);

  while(fraction < 1.0 && attempts < maxtries)
  {
    fraction = armgroup.computeCartesianPath(waypoints, eef_step, jump_threshold, trajectory);
    attempts++;
      
    if(attempts % 10 == 0)
      ROS_INFO("Still trying after %d attempts...", attempts);
  }
  
  if(fraction == 1)
  {   
    ROS_INFO("Path computed successfully. Moving the arm.");

    //generate planning path
    moveit::planning_interface::MoveGroupInterface::Plan plan;
    plan.trajectory_ = trajectory;

    //excute
    armgroup.execute(plan);
    sleep(1);
  }
  else
  {
    ROS_INFO("Path planning failed with only %0.6f success after %d attempts.", fraction, maxtries);
  }

}

//go to the specific position function
void GraspingDemo::attainPosition(float x, float y, float z)
{
  //get the name of the end link
  std::string end_effector_link = armgroup.getEndEffectorLink();

  //set the reference coordinate
  std::string reference_frame = "base_link";
  armgroup.setPoseReferenceFrame(reference_frame);
  
  //ROS_INFO("The attain position function called");
  std::vector<geometry_msgs::Pose> waypoints;

  geometry_msgs::PoseStamped currPose = armgroup.getCurrentPose();

  geometry_msgs::Pose target_pose1, target_pose2;

  target_pose1 = currPose.pose;
  
  target_pose1.position.z = currPose.pose.position.z + 0.001; 

  target_pose2.orientation = currPose.pose.orientation;
  target_pose2.position.x = x;
  target_pose2.position.y = y;
  target_pose2.position.z = z;

  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);

  straightPath(waypoints);
}

//approach object function
void GraspingDemo::attainObject(geometry_msgs::PoseStamped general_graspPose)
{
  //get the name of the end link
  std::string end_effector_link = armgroup.getEndEffectorLink();

  geometry_msgs::PoseStamped currPose = armgroup.getCurrentPose();
  geometry_msgs::Pose target_pose1, target_pose2;
  std::vector<geometry_msgs::Pose> waypoints;

  target_pose1 = currPose.pose;
  target_pose1.position.z = currPose.pose.position.z + 0.001;

  target_pose2 = transform_end (general_graspPose.pose);
  target_pose2.position.z = target_pose2.position.z + 0.100;
  
  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);

  straightPath(waypoints);

  //open Gripper
  ROS_INFO("Open Gripper");
  //grippergroup.setNamedTarget("open");
  //grippergroup.move();

  ros::WallDuration(2.0).sleep();
}

//grasp action function
void GraspingDemo::grasp(geometry_msgs::PoseStamped general_graspPose)
{
  geometry_msgs::PoseStamped currPose = armgroup.getCurrentPose();
  geometry_msgs::Pose target_pose1, target_pose2;
  std::vector<geometry_msgs::Pose> waypoints;

  target_pose1 = currPose.pose;
  target_pose1.position.z = currPose.pose.position.z + 0.001;

  target_pose2 = transform_end (general_graspPose.pose);
  target_pose2.position.z = target_pose2.position.z - 0.013;
  
  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);

  straightPath(waypoints);

  //Close Gripper
  ROS_INFO("Close Gripper");
  //grippergroup.setNamedTarget("close");
  //grippergroup.move();

  ros::WallDuration(2.0).sleep();
}

//lift action function
void GraspingDemo::lift()
{
  geometry_msgs::PoseStamped currPose = armgroup.getCurrentPose();
  geometry_msgs::Pose target_pose1, target_pose2;
  std::vector<geometry_msgs::Pose> waypoints;

  target_pose1 = currPose.pose;
  target_pose1.position.z = currPose.pose.position.z + 0.001;

  target_pose2 = currPose.pose;
  target_pose2.position.z = target_pose2.position.z + 0.105;
  
  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);

  straightPath(waypoints);
}

//go to graspHome pose
void GraspingDemo::go_graspHome() 
{
  //get the name of the end link
  std::string end_effector_link = armgroup.getEndEffectorLink();

  //set the reference coordinate
  std::string reference_frame = "base_link";
  armgroup.setPoseReferenceFrame(reference_frame);
  
  std::vector<geometry_msgs::Pose> waypoints;

  geometry_msgs::PoseStamped currPose = armgroup.getCurrentPose();
  geometry_msgs::Pose target_pose1, target_pose2;

  target_pose1 = currPose.pose;
  target_pose1.position.z = currPose.pose.position.z + 0.001;

  target_pose2.position.x = graspHome_x;
  target_pose2.position.y = graspHome_y;
  target_pose2.position.z = graspHome_z;

  target_pose2.orientation.x = -0.482942;
  target_pose2.orientation.y = 0.516511;
  target_pose2.orientation.z = 0.483004;
  target_pose2.orientation.w = 0.516421;

  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);

  straightPath(waypoints);
}

//go to Home position
void GraspingDemo::goHome() 
{
  tf::Quaternion q;
  double roll, pitch, yaw;

  //Go to the position above the table
  go_graspHome();

  geometry_msgs::PoseStamped currPose = armgroup.getCurrentPose();
  std::cout<< " X-orientation of graspHome :" << currPose.pose.orientation.x << std::endl;
  std::cout<< " Y-orientation of graspHome :" << currPose.pose.orientation.y << std::endl;
  std::cout<< " Z-orientation of graspHome :" << currPose.pose.orientation.z << std::endl;
  std::cout<< " w-orientation of graspHome :" << currPose.pose.orientation.w << std::endl;

  //go to Home Position
  attainPosition(pregrasp_x, pregrasp_y, pregrasp_z);
}

//transform function
geometry_msgs::Pose GraspingDemo::transform_end(geometry_msgs::Pose graspPose)
{
  tf::Transform eelink_to_robot, object_to_robot, toolend_to_object, toolend_to_eelink;

  geometry_msgs::Pose realEnd;

  object_to_robot.setOrigin( tf::Vector3(graspPose.position.x, graspPose.position.y, graspPose.position.z) );
  object_to_robot.setRotation( tf::Quaternion(graspPose.orientation.x, graspPose.orientation.y, graspPose.orientation.z, graspPose.orientation.w) );

  toolend_to_object.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
  toolend_to_object.setRotation( tf::Quaternion(-0.5, -0.5, -0.5, 0.5) );
  
  toolend_to_eelink.setOrigin( tf::Vector3(0.22, 0.0, 0.0) );
  toolend_to_eelink.setRotation( tf::Quaternion(0, 0, 0, 1) );

  eelink_to_robot = object_to_robot * toolend_to_object * toolend_to_eelink.inverse();

  realEnd.position.x = eelink_to_robot.getOrigin().x();
  realEnd.position.y = eelink_to_robot.getOrigin().y();
  realEnd.position.z = eelink_to_robot.getOrigin().z();

  realEnd.orientation.x = eelink_to_robot.getRotation().getX();
  realEnd.orientation.y = eelink_to_robot.getRotation().getY();
  realEnd.orientation.z = eelink_to_robot.getRotation().getZ();
  realEnd.orientation.w = eelink_to_robot.getRotation().getW();

  return realEnd;

}

//get distance between two points
float GraspingDemo::getDistance(geometry_msgs::Point pointA, geometry_msgs::Point pointB )
{
    float distance;
    distance = powf((pointA.x - pointB.x),2) + powf((pointA.y - pointB.y),2) + powf((pointA.z - pointB.z),2);
    distance = sqrtf(distance); 

    return distance;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//task planning

//step1 paper grasping and placement
void GraspingDemo::step1_graspPaper( geometry_msgs::PoseStamped paper_graspPose )
{
  tf::Quaternion q;
  double roll, pitch, yaw;
  
  std::vector<geometry_msgs::Pose> waypoints;

  geometry_msgs::Pose target_pose0, target_pose1, target_pose2, target_pose3, target_pose4, target_pose5;
  geometry_msgs::PoseStamped real_graspPose;

  ROS_INFO_STREAM("Go tp the position above the table");
  go_graspHome();
  
  ROS_INFO_STREAM("Approaching the Object....");
  attainObject( paper_graspPose );
  
  ROS_INFO_STREAM("Attempting to Grasp the Object now..");
  grasp( paper_graspPose );

  ROS_INFO_STREAM("Lifting the Object....");
  lift();

  geometry_msgs::PoseStamped currPose = armgroup.getCurrentPose();
  
  currPose = armgroup.getCurrentPose();
  target_pose0 = currPose.pose;
  target_pose0.position.z = target_pose0.position.z + 0.001;
  
  //definition of waypont1
  target_pose1.orientation = box_graspPose2.pose.orientation;
  target_pose1.position.x = box_graspPose1.pose.position.x * (-400)/500 + box_graspPose2.pose.position.x * (500-(-400))/500;
  target_pose1.position.y = box_graspPose1.pose.position.y * (-400)/500 + box_graspPose2.pose.position.y * (500-(-400))/500;
  target_pose1.position.z = 0.45;

  target_pose1 = transform_end (target_pose1);
  
  //definition of waypont2
  target_pose2.orientation = box_graspPose2.pose.orientation;
  target_pose2.position.x = box_graspPose2.pose.position.x;
  target_pose2.position.y = box_graspPose2.pose.position.y;
  target_pose2.position.z = box_graspPose2.pose.position.z + 0.77 / 2;

  target_pose2 = transform_end (target_pose2);

  //definition of waypont3
  target_pose3.orientation = box_graspPose1.pose.orientation;
  target_pose3.position.x = box_graspPose1.pose.position.x;
  target_pose3.position.y = box_graspPose1.pose.position.y;
  target_pose3.position.z = box_graspPose1.pose.position.z + 0.05;
  target_pose3 = transform_end (target_pose3);
  
  waypoints.push_back(target_pose0);
  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);
  waypoints.push_back(target_pose3);
  ROS_INFO_STREAM("Put down paper....");
  straightPath(waypoints);

  ROS_INFO_STREAM("Open gripper....");
  ros::WallDuration(2.0).sleep();

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();  

  //press the paper
  target_pose4.orientation = box_graspPose3.pose.orientation;
  target_pose4.position.x = box_graspPose3.pose.position.x * 2/3 + box_graspPose2.pose.position.x * 1/3;
  target_pose4.position.y = box_graspPose3.pose.position.y * 2/3 + box_graspPose2.pose.position.y * 1/3;
  target_pose4.position.z = paper_graspPose.pose.position.z + 0.02;
  target_pose4 = transform_end (target_pose4);

  attainPosition(target_pose4.position.x, target_pose4.position.y, target_pose4.position.z + 0.2);
  attainPosition(target_pose4.position.x, target_pose4.position.y, target_pose4.position.z);

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();  

  target_pose5.orientation = box_graspPose3.pose.orientation;
  target_pose5.position.x = box_graspPose3.pose.position.x * 1/3 + box_graspPose2.pose.position.x * 2/3;
  target_pose5.position.y = box_graspPose3.pose.position.y * 1/3 + box_graspPose2.pose.position.y * 2/3;
  target_pose5.position.z = paper_graspPose.pose.position.z + 0.02;
  target_pose5 = transform_end (target_pose5);

  attainPosition(target_pose5.position.x, target_pose5.position.y, target_pose5.position.z + 0.2);
  attainPosition(target_pose5.position.x, target_pose5.position.y, target_pose5.position.z);

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();
  
}

//step2 first shoe grasping and placement
void GraspingDemo::step2_graspShoe( geometry_msgs::PoseStamped shoe_graspPose  )
{
  std::vector<geometry_msgs::Pose> waypoints;

  geometry_msgs::PoseStamped currPose;
  geometry_msgs::Pose target_pose0, target_pose1, target_pose2, target_pose3;

  ROS_INFO_STREAM("Go tp the position above the table");
  go_graspHome();
  
  ROS_INFO_STREAM("Approaching the Object....");
  attainObject( shoe_graspPose );

  //the height of table is paper grasp point's z coordinate  
  shoe_graspPose.pose.position.z = paper_graspPose.pose.position.z + 0.025;
  
  ROS_INFO_STREAM("Attempting to Grasp the Object now..");
  grasp(shoe_graspPose);

  ROS_INFO_STREAM("Lifting the Object....");
  lift();
  
  ROS_INFO_STREAM("Going back to graspHome position....");
  go_graspHome();

  currPose = armgroup.getCurrentPose();
  target_pose0 = currPose.pose;
  target_pose0.position.z = target_pose0.position.z + 0.001;

  //placement position
  target_pose1 = box_graspPose2.pose;
  target_pose1.position.z = box_graspPose2.pose.position.z + 0.100;
  target_pose1.position.y = box_graspPose2.pose.position.y - 0.010;
  target_pose2 = box_graspPose2.pose;
  target_pose2.position.z = box_graspPose2.pose.position.z + 0.025;
  target_pose2.position.y = box_graspPose2.pose.position.y - 0.010;

  target_pose1 = transform_end (target_pose1);
  target_pose2 = transform_end (target_pose2);
  
  waypoints.push_back(target_pose0);
  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);
  ROS_INFO_STREAM("Put down shoe....");
  straightPath(waypoints);

  //open Gripper
  ros::WallDuration(1.0).sleep();
  //grippergroup.setNamedTarget("open");
  //grippergroup.move();

  ROS_INFO_STREAM("Leaving the Object....");
  lift();

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();
  
}

//step3 paper folding
void GraspingDemo::step3_foldPaper()
{
  std::vector<geometry_msgs::Pose> waypoints;

  geometry_msgs::PoseStamped currPose, paper_graspPose1;
  geometry_msgs::Pose target_pose, target_pose1, target_pose2;

  double radius = 0.1;

  ROS_INFO_STREAM("Go tp the position above the table");
  go_graspHome();

  paper_graspPose1 = box_graspPose2;

  paper_graspPose1.pose.position.x = box_graspPose1.pose.position.x * (-radius)/0.45 + box_graspPose2.pose.position.x * (1-(-radius)/0.45);
  paper_graspPose1.pose.position.y = box_graspPose1.pose.position.y * (-radius)/0.45 + box_graspPose2.pose.position.y * (1-(-radius)/0.45);
  paper_graspPose1.pose.position.z = paper_graspPose.pose.position.z;
  
  ROS_INFO_STREAM("Approaching the Object....");
  attainObject( paper_graspPose1 );

  ROS_INFO_STREAM("Attempting to Grasp the Object now..");
  grasp(paper_graspPose1);

  ROS_INFO_STREAM("Lifting the Object....");
  lift();

  currPose = armgroup.getCurrentPose();
  target_pose = currPose.pose;
  target_pose.position.z = target_pose.position.z + 0.001;
  waypoints.push_back(target_pose);

  target_pose1.orientation = box_graspPose2.pose.orientation;

  //definition of the arc path
  for(double th=0.0; th<3.14; th=th+0.01)
  {
    
    target_pose1.position.x = box_graspPose1.pose.position.x * (-radius*cos(th))/0.45 + box_graspPose2.pose.position.x * (1-(-radius*cos(th))/0.45);
    target_pose1.position.y = box_graspPose1.pose.position.y * (-radius*cos(th))/0.45 + box_graspPose2.pose.position.y * (1-(-radius*cos(th))/0.45);
    target_pose1.position.z = box_graspPose2.pose.position.z + radius * sin(th);

    target_pose2 = transform_end (target_pose1);
    waypoints.push_back(target_pose2);
  }

  straightPath(waypoints);

  //open Gripper
  ros::WallDuration(1.0).sleep();
  //grippergroup.setNamedTarget("open");
  //grippergroup.move();

  //press the paper
  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();

  target_pose1.orientation = box_graspPose2.pose.orientation;
  target_pose1.position.x = box_graspPose3.pose.position.x * 2/3 + box_graspPose2.pose.position.x * 1/3;
  target_pose1.position.y = box_graspPose3.pose.position.y * 2/3 + box_graspPose2.pose.position.y * 1/3;
  target_pose1.position.z = box_graspPose2.pose.position.z - 0.05;
  target_pose1 = transform_end (target_pose1);

  attainPosition(target_pose1.position.x, target_pose1.position.y, target_pose1.position.z + 0.100);
  attainPosition(target_pose1.position.x, target_pose1.position.y, target_pose1.position.z);

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();
}

//step4 the other shoe manipulation
void GraspingDemo::step4_graspShoe( geometry_msgs::PoseStamped shoe_graspPose )
{
  std::vector<geometry_msgs::Pose> waypoints;

  geometry_msgs::PoseStamped currPose;
  geometry_msgs::Pose target_pose0, target_pose1, target_pose2;

  ROS_INFO_STREAM("Go tp the position above the table");
  go_graspHome();
  
  ROS_INFO_STREAM("Approaching the Object....");
  attainObject( shoe_graspPose );

  //the height of table is paper grasp point's z coordinate  
  shoe_graspPose.pose.position.z = paper_graspPose.pose.position.z + 0.025;

  ROS_INFO_STREAM("Attempting to Grasp the Object now..");
  grasp(shoe_graspPose);

  ROS_INFO_STREAM("Lifting the Object....");
  lift();

  ROS_INFO_STREAM("Going back to graspHome position....");
  go_graspHome();

  currPose = armgroup.getCurrentPose();
  target_pose0 = currPose.pose;
  target_pose0.position.z = target_pose0.position.z + 0.001;

  //placement position
  target_pose1 = box_graspPose3.pose;
  target_pose1.position.z = box_graspPose3.pose.position.z + 0.100;
  target_pose1.position.y = box_graspPose3.pose.position.y + 0.010;
  target_pose2 = box_graspPose3.pose;
  target_pose2.position.z = box_graspPose3.pose.position.z + 0.025;
  target_pose2.position.y = box_graspPose3.pose.position.y + 0.010;

  target_pose1 = transform_end (target_pose1);
  target_pose2 = transform_end (target_pose2);
  
  waypoints.push_back(target_pose0);
  waypoints.push_back(target_pose1);
  waypoints.push_back(target_pose2);
  ROS_INFO_STREAM("Put down shoe....");
  straightPath(waypoints);

  //open Gripper
  ros::WallDuration(1.0).sleep();
  //grippergroup.setNamedTarget("open");
  //grippergroup.move();

  ROS_INFO_STREAM("Lifting the Object....");
  lift();

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();

}

//step5 box closing
void GraspingDemo::step5_closeBox()
{
  std::vector<geometry_msgs::Pose> waypoints, waypoints1, waypoints2;

  geometry_msgs::PoseStamped currPose;
  geometry_msgs::Pose target_pose0, target_pose1, target_pose2, target_pose3, target_pose4, target_pose5, target_pose6, target_pose7, target_pose8;

  ROS_INFO_STREAM("Go to the position above the table");
  go_graspHome();
  
  ROS_INFO_STREAM("Approaching the Object....");
  attainObject(box_graspPose1);

  ROS_INFO_STREAM("Attempting to Grasp the Object now..");
  grasp(box_graspPose1);

  currPose = armgroup.getCurrentPose();
  target_pose0 = currPose.pose;
  target_pose0.position.z = target_pose0.position.z + 0.001;
  waypoints.push_back(target_pose0);

  float radius = getDistance(box_graspPose1.pose.position, box_graspPose3.pose.position);
  float th0 = 3.14;
  tf::Transform realEnd, object_to_robot, closeBox;

  //definition of the arc path
  for(float th=th0; th>2; th=th-0.01)
  {

    target_pose1 = box_graspPose1.pose;

    target_pose1.position.x = box_graspPose1.pose.position.x * (-radius*cos(th)+0.21)/0.45 + box_graspPose2.pose.position.x * (1-(-radius*cos(th)+0.21)/0.45);
    target_pose1.position.y = box_graspPose1.pose.position.y * (-radius*cos(th)+0.21)/0.45 + box_graspPose2.pose.position.y * (1-(-radius*cos(th)+0.21)/0.45);
    target_pose1.position.z = box_graspPose3.pose.position.z + radius * sin(th);
    
    object_to_robot.setOrigin( tf::Vector3(target_pose1.position.x, target_pose1.position.y, target_pose1.position.z) );
    object_to_robot.setRotation( tf::Quaternion(target_pose1.orientation.x, target_pose1.orientation.y, target_pose1.orientation.z, target_pose1.orientation.w) );

    closeBox.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
    closeBox.setRotation( tf::Quaternion(-sin((3.14-th)/2), 0, 0, cos((3.14-th)/2)));

    realEnd = object_to_robot * closeBox;

    target_pose2.position.x = realEnd.getOrigin().x();
    target_pose2.position.y = realEnd.getOrigin().y();
    target_pose2.position.z = realEnd.getOrigin().z();

    target_pose2.orientation.x = realEnd.getRotation().getX();
    target_pose2.orientation.y = realEnd.getRotation().getY();
    target_pose2.orientation.z = realEnd.getRotation().getZ();
    target_pose2.orientation.w = realEnd.getRotation().getW();

    target_pose3 = transform_end (target_pose2);

    waypoints.push_back(target_pose3);

  }

  straightPath(waypoints);

  currPose = armgroup.getCurrentPose();
  target_pose4 = currPose.pose;
  target_pose4.position.z = target_pose4.position.z + 0.001;
  waypoints1.push_back(target_pose4);

  target_pose5 = currPose.pose;
  target_pose5.position.x = box_graspPose1.pose.position.x * (-radius*cos(2)+0.21-0.22-0.15)/0.45 + box_graspPose2.pose.position.x * (1-(-radius*cos(2)+0.21-0.22-0.15)/0.45);
  target_pose5.position.y = box_graspPose1.pose.position.y * (-radius*cos(2)+0.21-0.22-0.15)/0.45 + box_graspPose2.pose.position.y * (1-(-radius*cos(2)+0.21-0.22-0.15)/0.45);
  target_pose5.position.z = target_pose5.position.z + 0.001;
  waypoints1.push_back(target_pose5);

  straightPath(waypoints1);

  //open Gripper
  ros::WallDuration(2.0).sleep();
  //grippergroup.setNamedTarget("open");
  //grippergroup.move();

  //away from the box cover
  currPose = armgroup.getCurrentPose();
  target_pose6 = currPose.pose;
  target_pose6.position.z = target_pose6.position.z + 0.001;
  waypoints2.push_back(target_pose6);

  target_pose7 = currPose.pose;
  target_pose7.position.x = box_graspPose1.pose.position.x * (-radius*cos(2)+0.21-0.22-0.13-0.2)/0.45 + box_graspPose2.pose.position.x * (1-(-radius*cos(2)+0.21-0.22-0.13-0.2)/0.45);
  target_pose7.position.y = box_graspPose1.pose.position.y * (-radius*cos(2)+0.21-0.22-0.13-0.2)/0.45 + box_graspPose2.pose.position.y * (1-(-radius*cos(2)+0.21-0.22-0.13-0.2)/0.45);
  target_pose7.position.z = target_pose7.position.z + 0.001;
  waypoints2.push_back(target_pose7);

  straightPath(waypoints2);

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();

  //press the box
  target_pose8 = box_graspPose3.pose;
  target_pose8.position.x = box_graspPose3.pose.position.x * 1/2 + box_graspPose2.pose.position.x * 1/2;
  target_pose8.position.y = box_graspPose3.pose.position.y * 1/2 + box_graspPose2.pose.position.y * 1/2;
  target_pose8 = transform_end (target_pose8);
  attainPosition(target_pose8.position.x, target_pose8.position.y, target_pose8.position.z+0.002);

  ROS_INFO_STREAM("Going back to home position....");
  go_graspHome();

}

//////////////////////////////////////////////////////////////////////////////////////////////

//whole task
void GraspingDemo::initiateGrasping()
{
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::WallDuration(3.0).sleep();

  //homePose1 = armgroup.getCurrentPose();
  
  ROS_INFO_STREAM("Step1_graspPaper...");
  step1_graspPaper( paper_graspPose );

  ROS_INFO_STREAM("Step2_graspShoe...");
  step2_graspShoe( shoe_graspPose );

  ROS_INFO_STREAM("Step3_foldPaper...");
  step3_foldPaper();

  ROS_INFO_STREAM("Step4_graspShoe...");
  step4_graspShoe( shoe_graspPose );

  ROS_INFO_STREAM("Step5_closeBox...");
  step5_closeBox();

  ROS_INFO_STREAM("Go home...");
  goHome();

  grasp_running = false;
}

////////////////////////////////////////////////////////////////////////////////////////////////

// main function
int main(int argc, char **argv)
{
  ros::init(argc, argv, "simple_grasping");
  ros::NodeHandle n;

  GraspingDemo simGrasp(n);
  ROS_INFO_STREAM("Waiting for five seconds..");

  ros::WallDuration(5.0).sleep();
  while (ros::ok())
  {
    ros::spinOnce();

    simGrasp.initiateGrasping();
  }
  return 0;
}
