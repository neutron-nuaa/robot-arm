/***********************************************************************
Author: Dong Yi
Contact: dongyi@nuaa.edu.cn
Update: 22/02/2022
***********************************************************************/

#ifndef ur_GRASPING_DEMO
#define ur_GRASPING_DEMO

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/PoseStamped.h>

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/robot_trajectory/robot_trajectory.h>

#include <cv_bridge/cv_bridge.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

class GraspingDemo
{
  public:
	
	GraspingDemo(ros::NodeHandle n_);

	void paper_imageCb(const geometry_msgs::PoseStamped &pt);
	void shoe_imageCb(const geometry_msgs::PoseStamped &pt);
	void box_imageCb(const geometry_msgs::PoseArray &pt);
	
	void initiateGrasping();
	
	void goHome();

	//go to the position above the table
	void go_graspHome();

  private:
	
	ros::NodeHandle nh_;
	
	moveit::planning_interface::MoveGroupInterface armgroup;
	
	image_transport::ImageTransport it_;
	
	image_transport::Subscriber image_sub_;

	cv_bridge::CvImagePtr cv_ptr;
	
	////////////////////////////////////////////////////////////////
	ros::Subscriber paper_graspPose_sub_;
	ros::Subscriber shoe_graspPose_sub_;
	ros::Subscriber box_graspPose_sub_;

	bool grasp_running;
	
	tf::TransformListener listener;

	///////////////////////////////////////////////////////////////////////////
	geometry_msgs::PointStamped camera_point, robot_point;

	geometry_msgs::PoseStamped paper_graspPose, shoe_graspPose;
	
	geometry_msgs::PoseStamped box_graspPose1, box_graspPose2, box_graspPose3;
	
	float pregrasp_x = -0.1, pregrasp_y = 0.49, pregrasp_z = 0.45;

	//position above the table 
	float graspHome_x = 0.49, graspHome_y = 0, graspHome_z = 0.45;

	//////////////////////////////////////////////////////////////////////////
	
	void straightPath(std::vector<geometry_msgs::Pose> waypoints);

	void attainPosition(float x, float y, float z);
	
	void attainObject(geometry_msgs::PoseStamped general_graspPose);
	
	void grasp(geometry_msgs::PoseStamped general_graspPose);
	
	void lift();

	geometry_msgs::Pose transform_end(geometry_msgs::Pose graspPose);

	float getDistance (geometry_msgs::Point pointA, geometry_msgs::Point pointB );

	/////////////////////////////////////////////////////////////////////////

	void step1_graspPaper( geometry_msgs::PoseStamped paper_graspPose );

	void step2_graspShoe( geometry_msgs::PoseStamped shoe_graspPose );

	void step3_foldPaper();

	void step4_graspShoe( geometry_msgs::PoseStamped shoe_graspPose );

	void step5_closeBox();

};

#endif
