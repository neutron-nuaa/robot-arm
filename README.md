# Robot Arm

Scientific research and engineering application about robot arm.

## Grasp_system_based_on_vision

Introduction of the whole grasp system based on vision, including harware and software.

The hardware includes UR5e robot, Realsense camera D415, soft gripper, gripper griver and pump.

The software includes UR robot driver, realsense-ros, serial communication and eye to hand calibration.

## Paper_manipulation_and_shoe_packaging

Based on the grasp system with vision, paper-like object manipulation and shoe packaging task was studied.

Contents: XXX.

Reference paper: XXX.


