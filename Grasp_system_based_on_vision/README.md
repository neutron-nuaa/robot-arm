# Grasp_system_based_on_vision

Introduction of the whole grasp system based on vision, including harware and software.

## Contents

[Hardware](#hardware)

[Software](#software)

<details><summary>[1. ROS Installation](#1-ros-installation)</summary>
</details>

<details><summary>[2. UR robot driver](#2-ur-robot-driver)</summary>

2.1: Install UR robot driver

2.2: Install a URCap on a e-Series robot

2.3: Extract calibration information

2.4: Bring up the robot
</details>

<details><summary>[3. Install Realsense](#3-install-realsense)</summary>

3.1: Download related packages

3.2: Install librealsense package

3.3: Install realsense-ros package
</details>

<details><summary>[4. Eye to hand calibration](#4-eye-to-hand-calibration)</summary>

4.1: Install related packages

4.2: Modify the calibration launch file

4.3: Start the calibration

4.4: Publish TF
</details>

<details><summary>[5. Serial communication](#5-serial-communication)</summary>
</details>

[Notes](#notes)

## Hardware

<div align="left">
<!-- <img src="https://gitlab.com/neutron-nuaa/robot-arm/-/blob/main/Grasp_system_based_on_vision/Grasp_system.png" width="200"> -->
<img src="https://gitlab.com/neutron-nuaa/robot-arm/-/raw/main/Grasp_system_based_on_vision/Grasp_system.jpg" width="400"> 
</div>

**Fig.1:** The grasp system with vision.

The hardware includes UR5e robot, Realsense camera D415, soft gripper, gripper griver and pump.


**Robot:** UR5e
- **UR website:** http://www.universal-robots.com/
- **UR robot manual:** https://www.universal-robots.cn/technical-files/


**Camera:** Realsense D415
- **Realsense website:** https://www.intelrealsense.com/
- **Realsense technical files:** https://dev.intelrealsense.com/docs


**Gripper:** Rochu GC-4FMA6V5/LS1–SMP2L–FCMR03 + PCU-SVN
- **Rochu website:** https://www.rochu.com/
- **Rochu manual:** https://www.rochu.com/chanpinshouce/


## Software

The software includes UR robot driver, realsense package, eye to hand calibration and serial communication.

### 1. ROS Installation

**ROS Installation:** http://wiki.ros.org/ROS/Installation


### 2. UR robot driver

**Detailed tutorial:** https://github.com/UniversalRobots/Universal_Robots_ROS_Driver

**2.1: Install UR robot driver**
```
# create a catkin workspace
$ mkdir -p catkin_ws/src && cd catkin_ws

# clone the driver
$ git clone https://github.com/UniversalRobots/Universal_Robots_ROS_Driver.git src/Universal_Robots_ROS_Driver

# clone fork of the description. This is currently necessary, until the changes are merged upstream.
$ git clone -b calibration_devel https://github.com/fmauch/universal_robot.git src/fmauch_universal_robot

# install dependencies
$ sudo apt update -qq
$ rosdep update
$ rosdep install --from-paths src --ignore-src -y

# build the workspace
$ catkin_make

# activate the workspace (ie: source it)
$ source devel/setup.bash
```


**2.2: Install a URCap on a e-Series robot**

**Tutorial:** https://github.com/UniversalRobots/Universal_Robots_ROS_Driver/blob/master/ur_robot_driver/doc/install_urcap_e_series.md

- URCaps installation
- Setup the IP address
- Insert the External Control program


**2.3: Extract calibration information**

For this, there exists a helper script:

```
$ roslaunch ur_calibration calibration_correction.launch robot_ip:=<robot_ip> target_filename:="${HOME}/my_robot_calibration.yaml"
```

For the parameter robot_ip insert the IP address on which the ROS pc can reach the robot. As target_filename provide an absolute path where the result will be saved to.

**2.4: Bring up the robot**

For this, there exists a helper script:

```
$ roslaunch ur_robot_driver <robot_type>_bringup.launch robot_ip:=192.168.56.101 kinematics_config:=$(rospack find ur_calibration)/etc/ur10_example_calibration.yaml
```

where <robot_type> is one of ur3, ur5, ur10, ur3e, ur5e, ur10e, ur16e. Note that in this example we load the calibration parameters for the robot "ur10_example".

### 3. Install Realsense

**Detailed tutorial:** http://neutron.manoonpong.com/perception-vision-realsense-set-up-tutorial/

**3.1: Download related packages**

If you want to use Realsense camera with ROS. You need to install two packages (librealsense & realsense-ros). Before that, you need to determine the edtion of librealsense package and realsense-ros package you want. Versions of the two packages need to correspond to each other.

Download librealsense package: https://github.com/IntelRealSense/librealsense/releases

Download realsense-ros package: https://github.com/IntelRealSense/realsense-ros/releases

**3.2: Install librealsense package**

Using the source code packages (recommend): https://github.com/IntelRealSense/librealsense/blob/master/doc/installation.md

**3.3: Install realsense-ros package**

Realsense-ros install method: https://github.com/IntelRealSense/realsense-ros#installation-instructions

### 4. Eye to hand calibration

**Detailed tutorial:** https://zhuanlan.zhihu.com/p/92339362 (Chinese tutorial)

**4.1: Install related packages**

- aruco_ros

```
cd ~/catkin_ws/src
git clone -b kinetic-devel https://github.com/pal-robotics/aruco_ros.git
cd ..
catkin_make
```

- vision_visp / visp_hand2eye_calibration

```
cd ~/catkin_ws/src
git clone -b kinetic-devel https://github.com/lagadic/vision_visp.git
cd ..
catkin_make --pkg visp_hand2eye_calibration
```

- easy_handeye

```
cd ~/catkin_ws/src
git clone https://github.com/IFL-CAMP/easy_handeye
cd ..
catkin_make
```

**4.2: Modify the calibration launch file**

- realsense node

```
<!-- start the realsense -->
<include file="$(find realsense2_camera)/launch/rs_camera.launch" />
```

- aruco_ros node

Download the aruco marker from the website (https://chev.me/arucogen/) and print it out.

**Notes:**

Dictionary must be Original Aruco.

Marker ID and Marker size are optional. Modify them in the launch file.

When printing, choose the original size, and measure the actual size of the print.

```
<!-- start ArUco -->
<node name="aruco_tracker" pkg="aruco_ros" type="single">
    <remap from="/camera_info" to="/camera/color/camera_info" />
    <remap from="/image" to="/camera/color/image_raw" />
    <param name="image_is_rectified" value="true"/>
    <param name="marker_size"        value="0.1"/>
    <param name="marker_id"          value="582"/>

    <param name="reference_frame"    value="camera_color_frame"/>
    <param name="camera_frame"       value="camera_color_frame"/>
    <param name="marker_frame"       value="camera_marker" />
</node>
```

- ur5e node

Change the real IP address of the robot

```
<!-- start the robot -->
<!-- calibration of the robot -->
<include file="$(find ur_calibration)/launch/calibration_correction.launch">
    <arg name="robot_ip" value="192.168.31.2" />
    <arg name="target_filename" value= "$(find ur_robot_driver)/config/my_robot_calibration.yaml"/>
</include>

<!-- bring up the real robot -->
<include file="$(find ur_robot_driver)/launch/ur5e_bringup.launch">
    <arg name="limited" value="true" />
    <arg name="robot_ip" value="192.168.31.2" />
    <arg name="kinematics_config" value= "$(find ur_robot_driver)/config/my_robot_calibration.yaml"/>
</include>

<!-- moveit -->
<include file="$(find ur5_e_moveit_config)/launch/ur5_e_moveit_planning_execution.launch">
    <arg name="limited" value="true" />
</include>
```

- easy_handeye node

```
<!-- start easy_handeye -->
<include file="$(find easy_handeye)/launch/calibrate.launch" >
    <arg name="namespace_prefix" value="$(arg namespace_prefix)" />
    <arg name="eye_on_hand" value="false" />

    <arg name="tracking_base_frame" value="camera_color_frame" />
    <arg name="tracking_marker_frame" value="camera_marker" />
    <arg name="robot_base_frame" value="base" />
    <arg name="robot_effector_frame" value="tool0_controller" />

    <!-- eye to hand: false -->
    <arg name="freehand_robot_movement" value="false" />
    <arg name="robot_velocity_scaling" value="0.5" />
    <arg name="robot_acceleration_scaling" value="0.2" />
</include>
```

**4.3: Start the calibration**

```
roslaunch easy_handeye eye_to_hand_calibration.launch
```

<div align="left">
<img src="https://gitlab.com/neutron-nuaa/robot-arm/-/raw/main/Grasp_system_based_on_vision/Eye_to_hand_calibration.png" width="800"> 
</div>

**Fig.2:** Eye to hand calibration screen shot.

**4.4: Publish TF**

```
roslaunch easy_handeye publish.launch

# View the TF
rosrun tf tf_echo /base /camera_color_frame
```

### 5. Serial communication

Serial communication is one of the methods to control the gripper. 

Install ros-serial package

```
sudo apt-get install ros-melodic-serial

```
or use source code
```
git clone https://github.com/wjwwood/serial.git

```

## Notes

- This is a rough tutorial. For detailed tutorials, see the links above in each section.
- There are many ros nodes in this tutorial. Make good use of .launch files when doing projects.
- Errors may occur during compilation of each part. You can find answer on Google/Github/Ros Wiki or contact with me.


